# Answer:
### CreateDTO:
private Long questionId;->(not null)  
private Long optionId;->(not null)
## FullDTO:
private Long id;->(not null)  
private Long questionId;->(not null)  
private Long optionId;->(not null)  
private Long resultId;->(not null)  
private Boolean isCorrect;->(not null)  
***

# Option:
### CreateDTO:
private String text;->(not null)  
private Boolean isCorrect;->(not null)  
### FullDTO:
private Long id;->(not null)  
private String text;->(not null)  
private Boolean isCorrect;->(not null)
***

# Question:
### CreateDTO:
private String text;->(not null)  
private Long authorId;->(not null)  
private List&lt;OptionCreateDto> options;
### FullDTO:
private Long id;->(not null)  
private String text;->(not null)  
private Long authorId;->(not null)  
private Status status;->(not null)  
private List&lt;OptionFullDto> options;
### PreviewDTO:
private Long id;->(not null)  
private String text;->(not null)
### UpdateDTO:
private Long id;->(not null)  
private String text;->(not null)  
private Long authorId;->(not null)  
private Status status;->(not null)  
private List&lt;OptionCreateDto> options;
***

#Result:
### CreateDTO:
private Long testId;->(not null)  
private Long userId;->(not null)  
private List&lt;AnswerCreateDto> answers;
### FullDTO:
private Long id;->(not null)  
private TestPreviewDto testPreview;  
private Long userId;->(not null)  
private Integer score;->(not null)  
private List&lt;AnswerFullDto> answers;
### PreviewDTO:
private Long id;->(not null)  
private TestPreviewDto testPreview;  
private Long userId;->(not null)  
private Integer score;->(not null)
***

# Test:
### CreateDTO:
private String name;->(not null)  
private Language language;->(not null)  
private Direction direction;->(not null)  
private List&lt;Long> questionIds;
### FullDTO:
private Long id;->(not null)  
private String name;->(not null)  
private Language language;->(not null)  
private Direction direction;->(not null)  
private Level level;->(not null)  
private Integer rating;->(not null)  
private Long authorId;->(not null)  
private List&lt;QuestionPreviewDto> questions;
### PreviewDTO:
private Long id;->(not null)  
private String name;->(not null)  
private Language language;->(not null)  
private Direction direction;->(not null)  
private Level level;->(not null)  
private Integer rating;->(not null)  
private Long authorId;->(not null)  
### UpdateDTO:
private Long id;->(not null)  
private String name;->(not null)  
private Direction direction;->(not null)  
private Level level;->(not null)  
private Integer rating;->(not null)  
private Long authorId;->(not null)  
private List&lt;Long> questionIds; 