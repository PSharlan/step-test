package by.itstep.test.mapper;

import by.itstep.test.dto.option.OptionCreateDto;
import by.itstep.test.dto.option.OptionFullDto;
import by.itstep.test.entity.OptionEntity;
import by.itstep.test.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Test;

import java.util.List;

import static by.itstep.test.mapper.OptionMapper.OPTION_MAPPER;
import static by.itstep.test.util.DtoGeneratorUtils.generateOptionCreateDto;
import static by.itstep.test.util.EntityGeneratorUtils.generationOption;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class OptionMapperTest extends AbstractIntegrationTest {

    @Test
    void shouldMapOptionEntityToFullDto() {
        //given
        OptionEntity optionEntity = generationOption();
        optionEntity.setId(2L);

        //when
        OptionFullDto fullDto = OPTION_MAPPER.mapToFullDto(optionEntity);

        //then
        assertEquals(fullDto.getId(), optionEntity.getId());
        assertEquals(fullDto.getIsCorrect(), optionEntity.getIsCorrect());
        assertEquals(fullDto.getText(), optionEntity.getText());
    }

    @Test
    void shouldOptionCreateDtoToEntity() {
        //given
        OptionCreateDto optionCreateDto = generateOptionCreateDto();

        //when
        OptionEntity optionEntity = OPTION_MAPPER.mapToEntity(optionCreateDto);

        //then
        assertEquals(optionEntity.getText(), optionCreateDto.getText());
        assertEquals(optionEntity.getIsCorrect(), optionCreateDto.getIsCorrect());
    }

    @Test
    void shouldListOptionCreateDtoToListOptionEntity() {
        //given
        List<OptionCreateDto> optionCreateDtos = List.of(generateOptionCreateDto());

        //when
        List<OptionEntity> optionEntities = OPTION_MAPPER.mapToEntityList(optionCreateDtos);

        //then
        assertNotNull(optionEntities);
        assertEquals(optionEntities.size(), optionCreateDtos.size());
        assertEquals(optionEntities.get(0).getText(), optionCreateDtos.get(0).getText());
        assertEquals(optionEntities.get(0).getIsCorrect(), optionCreateDtos.get(0).getIsCorrect());
    }
}
