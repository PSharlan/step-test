package by.itstep.test.mapper;

import by.itstep.test.dto.answer.AnswerCreateDto;
import by.itstep.test.dto.answer.AnswerFullDto;
import by.itstep.test.entity.AnswerEntity;
import by.itstep.test.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Test;

import java.util.List;

import static by.itstep.test.mapper.AnswerMapper.ANSWER_MAPPER;
import static by.itstep.test.util.DtoGeneratorUtils.generateAnswerCreateDto;
import static by.itstep.test.util.EntityGeneratorUtils.generateAnswerFullEntity;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class AnswerMapperTest extends AbstractIntegrationTest {

    @Test
    void shouldMapAnswerEntityToFullDto() {
        //given
        AnswerEntity answer = generateAnswerFullEntity();

        //when
        AnswerFullDto fullDto = ANSWER_MAPPER.mapToFullDto(answer);

        //then
        assertEquals(fullDto.getIsCorrect(), answer.getIsCorrect());
        assertEquals(fullDto.getOptionId(), answer.getChosenOption().getId());
        assertEquals(fullDto.getQuestionId(), answer.getQuestion().getId());
        assertEquals(fullDto.getResultId(), answer.getResult().getId());
    }

    @Test
    void shouldAnswerCreateDtoToEntity(){
        //given
        AnswerCreateDto answerCreateDto = generateAnswerCreateDto();

        //when
        AnswerEntity answerEntity = ANSWER_MAPPER.mapToEntity(answerCreateDto);

        //then
        assertNotNull(answerEntity.getQuestion());
        assertNotNull(answerEntity.getChosenOption());
        assertEquals(answerEntity.getChosenOption().getId(), answerCreateDto.getOptionId());
        assertEquals(answerEntity.getQuestion().getId(), answerCreateDto.getQuestionId());
    }

    @Test
    void shouldListAnswerCreateDtoToListAnswerEntity() {
        //given
        List<AnswerCreateDto> answerCreateDtoList = List.of(generateAnswerCreateDto());

        //when
        List<AnswerEntity> answerEntities = ANSWER_MAPPER.mapToEntityList(answerCreateDtoList);

        //then
        assertNotNull(answerEntities);
        assertEquals(answerCreateDtoList.size(), answerEntities.size());
        assertEquals(answerCreateDtoList.get(0).getOptionId(), answerEntities.get(0).getChosenOption().getId());
        assertEquals(answerCreateDtoList.get(0).getQuestionId(), answerEntities.get(0).getQuestion().getId());
    }
}
