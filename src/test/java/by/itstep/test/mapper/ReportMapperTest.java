package by.itstep.test.mapper;

import by.itstep.test.dto.report.ReportCreateDto;
import by.itstep.test.dto.report.ReportFullDto;
import by.itstep.test.dto.report.ReportPreviewDto;
import by.itstep.test.entity.QuestionReportEntity;
import by.itstep.test.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static by.itstep.test.mapper.QuestionReportMapper.QUESTION_REPORT_MAPPER;
import static by.itstep.test.util.DtoGeneratorUtils.generateReportCreateDto;
import static by.itstep.test.util.EntityGeneratorUtils.generateListReports;
import static by.itstep.test.util.EntityGeneratorUtils.generateReportEntity;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class ReportMapperTest extends AbstractIntegrationTest {

    @Test
    void shouldEntityToFullDto() {
        //given
        QuestionReportEntity report = generateReportEntity();

        //when
        ReportFullDto fullDto = QUESTION_REPORT_MAPPER.mapToFullDto(report);

        //then
        assertNotNull(fullDto);
        assertEquals(report.getAuthorId(), fullDto.getAuthorId());
        assertEquals(report.getText(), fullDto.getText());
        assertEquals(report.getReportStatus(), fullDto.getReportStatus());
        assertEquals(report.getQuestion().getId(), fullDto.getQuestionId());
    }

    @Test
    void shouldListEntityToListFullDto() {
        //given
        List<QuestionReportEntity> reports = generateListReports();

        //when
        List<ReportPreviewDto> previewDtos = QUESTION_REPORT_MAPPER.mapToListPreviewDto(reports);

        //then
        for (int i = 0; i < previewDtos.size(); i++) {
            assertEquals(reports.get(i).getAuthorId(), previewDtos.get(i).getAuthorId());
            assertEquals(reports.get(i).getText(), previewDtos.get(i).getText());
            assertEquals(reports.get(i).getReportStatus(), previewDtos.get(i).getReportStatus());
            assertEquals(reports.get(i).getQuestion().getId(), previewDtos.get(i).getQuestionId());
        }
    }

    @Test
    void shouldCreateDtoToEntity() {
        //given
        ReportCreateDto createDto = generateReportCreateDto();

        //when
        QuestionReportEntity report = QUESTION_REPORT_MAPPER.mapToEntity(createDto);

        //then
        assertNotNull(report);
        assertEquals(createDto.getText(), report.getText());
        assertEquals(createDto.getAuthorId(), report.getAuthorId());
    }

    @Test
    void shouldEntityToPreviewDto() {
        //given
        QuestionReportEntity report = generateReportEntity();

        //when
        ReportPreviewDto previewDto = QUESTION_REPORT_MAPPER.mapToPreviewDto(report);

        //then
        assertNotNull(previewDto);
        assertEquals(report.getAuthorId(), previewDto.getAuthorId());
        assertEquals(report.getReportStatus(), previewDto.getReportStatus());
        assertEquals(report.getQuestion().getId(), previewDto.getQuestionId());
        assertEquals(report.getText(), previewDto.getText());
    }

}
