package by.itstep.test.mapper;

import by.itstep.test.dto.test.TestCreateDto;
import by.itstep.test.dto.test.TestFullDto;
import by.itstep.test.dto.test.TestPreviewDto;
import by.itstep.test.entity.TestEntity;
import by.itstep.test.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Test;

import java.util.List;

import static by.itstep.test.mapper.TestMapper.TEST_MAPPER;
import static by.itstep.test.util.DtoGeneratorUtils.generateTestCreateDto;
import static by.itstep.test.util.EntityGeneratorUtils.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class TestMapperTest extends AbstractIntegrationTest {

    @Test
    void shouldTestEntityToFullDto(){
        //given
        TestEntity testEntity = generateTestFullEntity();

        //when
        TestFullDto testFullDto = TEST_MAPPER.mapToFullDto(testEntity);

        //then
        assertEquals(testEntity.getName(), testFullDto.getName());
        assertEquals(testEntity.getLanguage(), testFullDto.getLanguage());
        assertEquals(testEntity.getQuestions().size(), testFullDto.getQuestions().size());
    }

    @Test
    void shouldTestEntityToPreviewDto(){
        //given
        TestEntity testEntity = generateTest();
        testEntity.setId(1L);
        testEntity.setQuestions(generateListQuestion());

        //when
        TestPreviewDto testPreviewDto = TEST_MAPPER.mapToPreviewDto(testEntity);

        //then
        assertEquals(testPreviewDto.getId(), testEntity.getId());
        assertEquals(testPreviewDto.getNumberOfQuestions(), testEntity.getQuestions().size());
        assertEquals(testPreviewDto.getAuthorId(), testEntity.getAuthorId());
        assertEquals(testPreviewDto.getLanguage(), testEntity.getLanguage());
        assertEquals(testPreviewDto.getDirection(), testEntity.getDirection());
        assertEquals(testPreviewDto.getLevel(), testEntity.getLevel());
        assertEquals(testPreviewDto.getName(), testEntity.getName());
        assertEquals(testPreviewDto.getRating(), testEntity.getRating());
    }

    @Test
    void shouldTestCreateDtoToEntity() {
        ///given
        TestCreateDto testCreateDto = generateTestCreateDto();

        //when
        TestEntity testEntity = TEST_MAPPER.mapToEntity(testCreateDto);

        //then
        assertEquals(testCreateDto.getName(), testEntity.getName());
        assertEquals(testCreateDto.getAuthorId(), testEntity.getAuthorId());
        assertEquals(testCreateDto.getDirection(), testEntity.getDirection());
        assertEquals(testCreateDto.getLanguage(), testEntity.getLanguage());
    }

    @Test
    void shouldListTestEntityToListPreviewDto() {
        //given
        List<TestEntity> testEntities = List.of(generateTestFullEntity());
        testEntities.forEach(testEntity -> testEntity.setQuestions(generateListQuestion()));

        //when
        List<TestPreviewDto> testPreviewDtos = TEST_MAPPER.mapToPreviewDtoList(testEntities);

        //then
        assertNotNull(testPreviewDtos);
        assertEquals(testPreviewDtos.size(), testEntities.size());
        for(int i = 0; i < testPreviewDtos.size(); i++) {
            assertEquals(testPreviewDtos.get(i).getId(), testEntities.get(i).getId());
            assertEquals(testPreviewDtos.get(i).getNumberOfQuestions(), testEntities.get(i).getQuestions().size());
            assertEquals(testPreviewDtos.get(i).getAuthorId(), testEntities.get(i).getAuthorId());
            assertEquals(testPreviewDtos.get(i).getDirection(), testEntities.get(i).getDirection());
            assertEquals(testPreviewDtos.get(i).getName(), testEntities.get(i).getName());
            assertEquals(testPreviewDtos.get(i).getLanguage(), testEntities.get(i).getLanguage());
            assertEquals(testPreviewDtos.get(i).getRating(), testEntities.get(i).getRating());
        }
    }
}
