package by.itstep.test.mapper;

import by.itstep.test.dto.question.QuestionCreateDto;
import by.itstep.test.dto.question.QuestionFullDto;
import by.itstep.test.dto.question.QuestionPreviewDto;
import by.itstep.test.entity.QuestionEntity;
import by.itstep.test.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;


import java.util.List;

import static by.itstep.test.mapper.QuestionMapper.QUESTION_MAPPER;
import static by.itstep.test.util.DtoGeneratorUtils.*;
import static by.itstep.test.util.EntityGeneratorUtils.generateQuestionFullEntity;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class QuestionMapperTest extends AbstractIntegrationTest {

    @Test
    void shouldQuestionEntityToFullDto() {
        //given
        QuestionEntity entity = generateQuestionFullEntity();

        //when
        QuestionFullDto fullDto = QUESTION_MAPPER.mapToFullDto(entity);

        //then
        assertEquals(entity.getId(), fullDto.getId());
        assertEquals(entity.getOptions().size(), fullDto.getOptions().size());
        assertEquals(entity.getAuthorId(), fullDto.getAuthorId());
        assertEquals(entity.getText(), fullDto.getText());
        assertEquals(entity.getStatus(), fullDto.getStatus());
    }

    @Test
    void shouldQuestionEntityToPreviewDto() {
        //given
        QuestionEntity entity = generateQuestionFullEntity();

        //when
        QuestionPreviewDto previewDto = QUESTION_MAPPER.mapToPreviewDto(entity);

        //then
        assertEquals(entity.getId(), previewDto.getId());
        assertEquals(entity.getText(), previewDto.getText());
    }

    @Test
    void shouldQuestionCreateDtoToQuestionEntity() {
        //given
        QuestionCreateDto createDto = generateQuestionCreateDto();

        //when
        QuestionEntity entity = QUESTION_MAPPER.mapToEntity(createDto);

        //then
        assertEquals(entity.getOptions().size(), createDto.getOptions().size());
        assertEquals(entity.getText(), createDto.getText());
        assertEquals(entity.getAuthorId(), createDto.getAuthorId());
    }

    @Test
    void shouldListQuestionEntityToListQuestionPreviewDto() {
        //given
        List<QuestionEntity> questionEntities = List.of(generateQuestionFullEntity());

        //when
        List<QuestionPreviewDto> questionPreviewDtos = QUESTION_MAPPER.mapToPreviewDtoList(questionEntities);

        //then
        assertNotNull(questionPreviewDtos);
        assertEquals(questionPreviewDtos.size(), questionEntities.size());
        assertEquals(questionPreviewDtos.get(0).getId(), questionEntities.get(0).getId());
        assertEquals(questionPreviewDtos.get(0).getText(), questionEntities.get(0).getText());
    }
}
