package by.itstep.test.mapper;

import by.itstep.test.dto.result.ResultCreateDto;
import by.itstep.test.dto.result.ResultFullDto;
import by.itstep.test.dto.result.ResultPreviewDto;
import by.itstep.test.entity.ResultEntity;
import by.itstep.test.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static by.itstep.test.mapper.ResultMapper.RESULT_MAPPER;
import static by.itstep.test.util.DtoGeneratorUtils.*;
import static by.itstep.test.util.EntityGeneratorUtils.generateResultFullEntity;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class ResultMapperTest extends AbstractIntegrationTest {

    @Test
    void shouldResultEntityToFullDto() {
        //given
        ResultEntity resultEntity = generateResultFullEntity();

        //when
        ResultFullDto fullDto = RESULT_MAPPER.mapToFullDto(resultEntity);

        //then
        assertEquals(resultEntity.getId(), fullDto.getId());
        assertEquals(resultEntity.getScore(), fullDto.getScore());
        assertEquals(resultEntity.getUserId(), fullDto.getUserId());
        assertEquals(resultEntity.getTimeSpent(), fullDto.getTimeSpent());
        assertEquals(resultEntity.getAnswers().size(), fullDto.getAnswers().size());
    }

    @Test
    void shouldResultEntityToPreviewDto() {
        //given
        ResultEntity resultEntity = generateResultFullEntity();

        //when
        ResultPreviewDto previewDto = RESULT_MAPPER.mapToPreviewDto(resultEntity);

        //then
        assertEquals(resultEntity.getUserId(), previewDto.getUserId());
        assertEquals(resultEntity.getTest().getId(), previewDto.getTestPreview().getId());
        assertEquals(resultEntity.getUserId(), previewDto.getUserId());
        assertEquals(resultEntity.getScore(), previewDto.getScore());
        assertEquals(resultEntity.getTimeSpent(), previewDto.getTimeSpent());
    }

    @Test
    void shouldResultCreateDtoToEntity() {
        //given
        ResultCreateDto createDto = generateResultCreateDto();

        //when
        ResultEntity resultEntity = RESULT_MAPPER.mapToEntity(createDto);

        //then
        assertEquals(createDto.getUserId(), resultEntity.getUserId());
        assertEquals(createDto.getTimeSpent(), resultEntity.getTimeSpent());
        assertEquals(createDto.getAnswers().size(), resultEntity.getAnswers().size());
    }

    @Test
    void shouldListResultEntityToListResultPreviewDto() {
        //given
        List<ResultEntity> answerEntityList = List.of(generateResultFullEntity());

        //when
        List<ResultPreviewDto> answerPreviewDtos = RESULT_MAPPER.mapToPreviewDtoList(answerEntityList);

        //then
        assertNotNull(answerPreviewDtos);
        assertEquals(answerEntityList.size(), answerPreviewDtos.size());
        assertEquals(answerEntityList.get(0).getId(), answerPreviewDtos.get(0).getId());
        assertEquals(answerEntityList.get(0).getUserId(), answerPreviewDtos.get(0).getUserId());
        assertEquals(answerEntityList.get(0).getScore(), answerPreviewDtos.get(0).getScore());
    }
}
