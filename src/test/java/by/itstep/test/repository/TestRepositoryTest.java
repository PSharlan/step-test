package by.itstep.test.repository;

import by.itstep.test.entity.ResultEntity;
import by.itstep.test.entity.TestEntity;
import by.itstep.test.entity.enums.Direction;
import by.itstep.test.entity.enums.Language;
import by.itstep.test.entity.enums.Level;
import by.itstep.test.exception.TestNotFoundException;
import by.itstep.test.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Test;

import java.text.DecimalFormat;
import java.util.List;

import static by.itstep.test.util.EntityGeneratorUtils.*;
import static org.junit.jupiter.api.Assertions.*;

public class TestRepositoryTest  extends AbstractIntegrationTest {

    @Test
    void findById_happyPath() {
        // given
        TestEntity saved = testRepository.save(generateTest());

        // when
        TestEntity found = testRepository.findById(saved.getId())
                .orElseThrow(() -> new TestNotFoundException(saved.getId()));

        // then
        assertNotNull(found);
        assertEquals(saved.getId(), found.getId());
        assertEquals(saved.getLevel(), found.getLevel());
        assertEquals(saved.getRating(), found.getRating());
        assertEquals(saved.getAuthorId(), found.getAuthorId());
        assertEquals(saved.getDirection(), found.getDirection());
    }

    @Test
    void findAll_happyPath() {
        // given
        List<TestEntity> savedTests = testRepository.saveAll(generateListTest());

        // when
        List<TestEntity> foundTests = testRepository.findAll();

        // then
        assertNotNull(foundTests);
        assertEquals(savedTests.size(), foundTests.size());
        for (int i = 0; i < savedTests.size(); i++) {
            assertEquals(savedTests.get(i).getLevel(), foundTests.get(i).getLevel());
            assertEquals(savedTests.get(i).getRating(), foundTests.get(i).getRating());
            assertEquals(savedTests.get(i).getLanguage(), foundTests.get(i).getLanguage());
            assertEquals(savedTests.get(i).getAuthorId(), foundTests.get(i).getAuthorId());
            assertEquals(savedTests.get(i).getDirection(), foundTests.get(i).getDirection());
        }
    }

    @Test
    void create_happyPath() {
        //given
        TestEntity toSave = generateTest();

        //when
        TestEntity saved = testRepository.save(toSave);
        TestEntity found = testRepository.findById(saved.getId())
                .orElseThrow(() -> new TestNotFoundException(saved.getId()));

        //then
        assertNotNull(saved.getId());
        assertEquals(saved.getId(), found.getId());
    }

    @Test
    void update_happyPath() {
        //given
        TestEntity saved = testRepository.save(generateTest());

        TestEntity found = testRepository.findById(saved.getId())
                .orElseThrow(() -> new TestNotFoundException(saved.getId()));

        saved.setAuthorId(3L);
        saved.setLevel(Level.MEDIUM);
        saved.setDirection(Direction.FRONTEND);
        saved.setLanguage(Language.JAVASCRIPT);
        saved.setName("Angular");

        //when
        testRepository.save(saved);

        TestEntity foundAfterUpdated = testRepository.findById(saved.getId())
                .orElseThrow(() -> new TestNotFoundException(saved.getId()));

        //then
        assertNotNull(found);
        assertNotNull(foundAfterUpdated);
        assertEquals(found.getId(), foundAfterUpdated.getId());
        assertNotEquals(found.getName(), foundAfterUpdated.getName());
        assertEquals(found.getRating(), foundAfterUpdated.getRating());
        assertNotEquals(found.getLevel(), foundAfterUpdated.getLevel());
        assertNotEquals(found.getLanguage(), foundAfterUpdated.getLanguage());
        assertNotEquals(found.getAuthorId(), foundAfterUpdated.getAuthorId());
        assertNotEquals(found.getDirection(), foundAfterUpdated.getDirection());
    }

    @Test
    void delete_happyPath() {
        //given
        TestEntity save = testRepository.save(generateTest());

        //when
        testRepository.deleteById(save.getId());
        TestEntity found = testRepository.findById(save.getId()).orElse(new TestEntity());

        //then
        assertNull(found.getId());
    }

    @Test
    void getAverageScore_happyPath() {
        //given
        TestEntity saveTest = testRepository.save(generateTest());

        double average = getResults(saveTest)
                .stream()
                .mapToInt(ResultEntity::getScore)
                .average().getAsDouble();

        Double avSc = Double.parseDouble(new DecimalFormat("##.00").format(average));

        //when
        Double averageScore = testRepository.getAverageScore(saveTest.getId()).orElse(0D);

        //then
        assertNotNull(averageScore);
        assertEquals(averageScore, avSc);
    }

    private List<ResultEntity> getResults(TestEntity test) {
        ResultEntity resultToSave = generateResult();

        ResultEntity resultTwoToSave = generateResult();
        resultTwoToSave.setScore(3);

        ResultEntity resultThreeToSave = generateResult();
        resultThreeToSave.setScore(4);

        List<ResultEntity> results = List.of(resultToSave, resultTwoToSave, resultThreeToSave);
        results.forEach(r -> r.setTest(test));

        return resultRepository.saveAll(results);
    }

    @Test
    void getAverageScore_whenResultsNull() {
        //given
        TestEntity save = testRepository.save(generateTest());

        //when
        Double averageScore = testRepository.getAverageScore(save.getId()).orElse(0D);

        //then
        assertNotNull(averageScore);
        assertEquals(averageScore, 0);
    }

    @Test
    void getNumberOfPassed_whenResultsNull() {
        //given
        TestEntity save = testRepository.save(generateTest());

        //when
        Integer numberOfPassed = testRepository.getNumberPassed(save.getId()).orElse(0);

        //then
        assertNotNull(numberOfPassed);
        assertEquals(numberOfPassed, 0);
    }

    @Test
    void getNumberOfPassed_happyPath() {
        //given
        TestEntity saveTest = testRepository.save(generateTest());

        ResultEntity resultToSave = generateResult();
        resultToSave.setTest(saveTest);
        ResultEntity resultSaved = resultRepository.save(resultToSave);

        //when
        Integer numberOfPassed = testRepository.getNumberPassed(saveTest.getId()).orElse(0);

        //then
        assertNotNull(numberOfPassed);
        assertEquals(numberOfPassed, 1);
    }
}
