package by.itstep.test.repository;

import by.itstep.test.entity.OptionEntity;
import by.itstep.test.entity.QuestionEntity;
import by.itstep.test.exception.QuestionNotFoundException;
import by.itstep.test.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Test;

import java.util.List;

import static by.itstep.test.util.EntityGeneratorUtils.*;
import static org.junit.jupiter.api.Assertions.*;

public class QuestionRepositoryTest extends AbstractIntegrationTest {

    @Test
    void findById_happyPath() {
        //given
        QuestionEntity saved = questionRepository.save(generateQuestion());

        //when
        QuestionEntity found = questionRepository.findById(saved.getId())
                .orElseThrow(() -> new QuestionNotFoundException(saved.getId()));

        //then
        assertNotNull(found);
        assertEquals(saved.getId(), found.getId());
    }

    @Test
    void findAll_happyPath() {
        //given
        List<QuestionEntity> savedOptions = questionRepository.saveAll(generateListQuestion());

        //when
        List<QuestionEntity> foundOptions = questionRepository.findAll();

        //then
        assertNotNull(foundOptions);
        assertEquals(savedOptions.size(), foundOptions.size());
    }

    @Test
    void create_happyPath() {
        //given
        QuestionEntity toSave = generateQuestion();
        List<OptionEntity> options = generateListOption();

        toSave.setOptions(options);
        for(OptionEntity option : options) {
            option.setQuestion(toSave);
        }

        //when
        QuestionEntity saved = questionRepository.save(toSave);
        QuestionEntity found = questionRepository.findById(saved.getId())
                .orElseThrow(() -> new QuestionNotFoundException(saved.getId()));

        //then
        assertNotNull(saved.getId());
        assertEquals(saved.getId(), found.getId());
        assertEquals(saved.getOptions().size(), found.getOptions().size());
        for (int i = 0; i < found.getOptions().size(); i++) {
            assertEquals(found.getOptions().get(i).getQuestion(), saved.getOptions().get(i).getQuestion());
            assertEquals(found.getOptions().get(i).getIsCorrect(), saved.getOptions().get(i).getIsCorrect());
            assertEquals(found.getOptions().get(i).getId(), saved.getOptions().get(i).getId());
            assertEquals(found.getOptions().get(i).getText(), saved.getOptions().get(i).getText());
        }
    }

    @Test
    void update_happyPath() {
        //given
        QuestionEntity toSave = generateQuestion();
        QuestionEntity saved = questionRepository.save(toSave);
        QuestionEntity found = questionRepository.findById(saved.getId())
                .orElseThrow(() -> new QuestionNotFoundException(saved.getId()));

        //when
        saved.setText("Нет");
        questionRepository.save(toSave);
        QuestionEntity foundAfterUpdated = questionRepository.findById(saved.getId())
                .orElseThrow(() -> new QuestionNotFoundException(saved.getId()));

        //then
        assertEquals(found.getId(), foundAfterUpdated.getId());
        assertNotEquals(found.getText(), foundAfterUpdated.getText());
    }

    @Test
    void delete_happyPath() {
        //given
        QuestionEntity saved = questionRepository.save(generateQuestion());

        //when
        questionRepository.deleteById(saved.getId());
        QuestionEntity found = questionRepository.findById(saved.getId()).orElse(new QuestionEntity());

        //then
        assertNull(found.getId());
        assertNull(found.getText());
        assertNull(found.getStatus());
        assertNull(found.getAuthorId());
    }
}
