package by.itstep.test.repository;

import by.itstep.test.dto.result.ResultCreateDto;
import by.itstep.test.dto.result.ResultFullDto;
import by.itstep.test.entity.*;
import by.itstep.test.entity.enums.Action;
import by.itstep.test.exception.ResultNotFoundException;
import by.itstep.test.integration.AbstractIntegrationTest;

import by.itstep.test.service.StepAchievementsClient;
import by.itstep.test.service.impl.ResultServiceImpl;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.List;

import static by.itstep.test.util.DtoGeneratorUtils.generateResultCreateDto;
import static by.itstep.test.util.EntityGeneratorUtils.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class ResultRepositoryTest extends AbstractIntegrationTest {

    @Test
    void findById_happyPath() {
        //given
        ResultEntity saved = resultRepository.save(generateResult());

        //when
        ResultEntity found = resultRepository.findById(saved.getId())
                .orElseThrow(() -> new ResultNotFoundException(saved.getId()));

        //then
        assertNotNull(found);
        assertEquals(saved.getId(), found.getId());
        assertEquals(saved.getUserId(), found.getUserId());
        assertNotNull(found.getAnswers());
    }

    @Test
    void findByUserIdAndTestId_happyPath() {
        //given
        when(achievementsClient.recordAchievement(any(Action.class), any(Long.class)))
                .thenReturn(playerProgressFullDto);
        ResultFullDto result = resultService.create(getResultCreateDto());

        //when
        List<ResultEntity> results = resultRepository
                .getResultByUserAndTestId(result.getUserId(),
                                            result.getTestPreview().getId());

        //then
        assertNotNull(results);
        assertEquals(results.get(0).getUserId(), result.getUserId());
        assertEquals(results.get(0).getTest().getId(), result.getTestPreview().getId());

    }

    @Test
    void findAll_happyPath() {
        //given
        List<ResultEntity> saved = resultRepository.saveAll(generateListResults());

        //when
        List<ResultEntity> found = resultRepository.findAll();

        //then
        assertNotNull(found);
        assertEquals(saved.size(), found.size());
        for(int i = 0; i < found.size(); i++) {
            assertEquals(found.get(i).getId(), saved.get(i).getId());
            assertEquals(found.get(i).getUserId(), saved.get(i).getUserId());
            assertEquals(found.get(i).getScore(), saved.get(i).getScore());
            assertEquals(found.get(i).getTest(), saved.get(i).getTest());
        }
    }

    @Test
    void create_happyPath() {
        //given
        ResultEntity toSave = generateResult();
        List<AnswerEntity> answers = generateListAnswers();

        toSave.setAnswers(answers);
        for (AnswerEntity answer : answers) {
            answer.setResult(toSave);
        }

        //when
        ResultEntity saved = resultRepository.save(toSave);
        ResultEntity found = resultRepository.findById(saved.getId())
                .orElseThrow(() -> new ResultNotFoundException(saved.getId()));

        //then
        assertNotNull(saved.getId());
        assertEquals(saved.getId(), found.getId());
        assertEquals(found.getAnswers().size(), saved.getAnswers().size());
    }

    @Test
    void update_happyPath() {
        //given
        ResultEntity saved = resultRepository.save(generateResult());
        ResultEntity found = resultRepository.findById(saved.getId())
                .orElseThrow(() -> new ResultNotFoundException(saved.getId()));
        saved.setUserId(2L);

        //when
        resultRepository.save(saved);
        ResultEntity foundAfterUpdated = resultRepository.findById(saved.getId())
                .orElseThrow(() -> new ResultNotFoundException(saved.getId()));

        //then
        assertEquals(found.getId(), foundAfterUpdated.getId());
        assertNotEquals(found.getUserId(), foundAfterUpdated.getUserId());
    }

    @Test
    void delete_happyPath() {
        //given
        ResultEntity saved = resultRepository.save(generateResult());

        //when
        resultRepository.deleteById(saved.getId());
        ResultEntity found = resultRepository.findById(saved.getId()).orElse(new ResultEntity());

        //then
        assertNull(found.getId());
        assertNull(found.getTest());
        assertNull(found.getUserId());
    }

    private ResultCreateDto getResultCreateDto() {
        TestEntity test = testRepository.save(generateTest());

        List<QuestionEntity> questions = generateListQuestion();
        for (QuestionEntity question : questions) {
            List<OptionEntity> options = generateListOption();

            question.setOptions(options);
            for (OptionEntity option : options) {
                option.setQuestion(question);
            }
        }
        List<QuestionEntity> savedQuestion = questionRepository.saveAll(questions);

        return generateResultCreateDto(savedQuestion, test);
    }
}
