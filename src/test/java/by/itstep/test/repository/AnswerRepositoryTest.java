package by.itstep.test.repository;

import by.itstep.test.entity.AnswerEntity;
import by.itstep.test.exception.AnswerNotFoundException;
import by.itstep.test.integration.AbstractIntegrationTest;

import org.junit.jupiter.api.Test;


import java.util.List;

import static by.itstep.test.util.EntityGeneratorUtils.generateAnswer;
import static by.itstep.test.util.EntityGeneratorUtils.generateListAnswers;
import static org.junit.jupiter.api.Assertions.*;

public class AnswerRepositoryTest extends AbstractIntegrationTest {

    @Test
    void findById_happyPath() {
        // given
        AnswerEntity saved = answerRepository.save(generateAnswer());

        // when
        AnswerEntity found = answerRepository.findById(saved.getId())
                .orElseThrow(() -> new AnswerNotFoundException(saved.getId()));

        // then
        assertNotNull(found);
        assertEquals(saved.getId(), found.getId());
    }

    @Test
    void findAll_happyPath() {
        // given
        List<AnswerEntity> savedAnswers = answerRepository.saveAll(generateListAnswers());

        // when
        List<AnswerEntity> foundTests = answerRepository.findAll();

        // then
        assertEquals(savedAnswers.size(), foundTests.size());
        for (int i = 0; i < savedAnswers.size(); i++) {
            assertEquals(savedAnswers.get(i).getId(), foundTests.get(i).getId());
            assertEquals(savedAnswers.get(i).getIsCorrect(), foundTests.get(i).getIsCorrect());
            assertEquals(savedAnswers.get(i).getQuestion(), foundTests.get(i).getQuestion());
            assertEquals(savedAnswers.get(i).getResult(), foundTests.get(i).getResult());
            assertEquals(savedAnswers.get(i).getChosenOption(), foundTests.get(i).getChosenOption());
        }
    }

    @Test
    void create_happyPath() {
        //given
        AnswerEntity toSave = generateAnswer();

        //when
        AnswerEntity saved = answerRepository.save(toSave);
        AnswerEntity found = answerRepository.findById(saved.getId())
                .orElseThrow(() -> new AnswerNotFoundException(saved.getId()));

        //then
        assertNotNull(saved.getId());
        assertEquals(saved.getId(), found.getId());
    }

    @Test
    void update_happyPath() {
        //given
        AnswerEntity saved = answerRepository.save(generateAnswer());

        AnswerEntity found = answerRepository.findById(saved.getId())
                .orElseThrow(() -> new AnswerNotFoundException(saved.getId()));
        found.setIsCorrect(false);

        //when
        answerRepository.save(found);
        AnswerEntity foundAfterUpdated = answerRepository.findById(saved.getId())
                .orElseThrow(() -> new AnswerNotFoundException(saved.getId()));

        //then
        assertNotNull(found);
        assertNotNull(foundAfterUpdated);
        assertEquals(saved.getId(), foundAfterUpdated.getId());
        assertNotEquals(saved.getIsCorrect(), foundAfterUpdated.getIsCorrect());
    }

    @Test
    void delete_happyPath() {
        //given
        AnswerEntity saved = answerRepository.save(generateAnswer());

        //when
        answerRepository.deleteById(saved.getId());
        AnswerEntity found = answerRepository.findById(saved.getId()).orElse(new AnswerEntity());

        //then
        assertNull(found.getId());
        assertNull(found.getIsCorrect());
        assertNull(found.getQuestion());
        assertNull(found.getChosenOption());
        assertNull(found.getResult());
    }
}
