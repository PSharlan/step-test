package by.itstep.test.repository;

import by.itstep.test.entity.OptionEntity;
import by.itstep.test.exception.OptionNotFoundException;
import by.itstep.test.integration.AbstractIntegrationTest;

import org.junit.jupiter.api.Test;

import java.util.List;

import static by.itstep.test.util.EntityGeneratorUtils.generateListOption;
import static by.itstep.test.util.EntityGeneratorUtils.generationOption;
import static org.junit.jupiter.api.Assertions.*;

public class OptionRepositoryTest extends AbstractIntegrationTest {

    @Test
    void findById_happyPath() {
        //given
        OptionEntity saved = optionRepository.save(generationOption());

        //when
        OptionEntity found = optionRepository.findById(saved.getId())
                .orElseThrow(() -> new OptionNotFoundException(saved.getId()));

        //then
        assertEquals(saved.getId(), found.getId());
        assertEquals(saved.getIsCorrect(), found.getIsCorrect());
        assertEquals(saved.getText(), found.getText());
    }

    @Test
    void findAll_happyPath() {
        //given
        List<OptionEntity> savedOptions = optionRepository.saveAll(generateListOption());

        //when
        List<OptionEntity> foundOptions = optionRepository.findAll();

        //then
        assertNotNull(foundOptions);
        assertEquals(savedOptions.size(), foundOptions.size());
        for (int i = 0; i < foundOptions.size(); i++) {
            assertEquals(foundOptions.get(i).getId(), savedOptions.get(i).getId());
            assertEquals(foundOptions.get(i).getText(), savedOptions.get(i).getText());
            assertEquals(foundOptions.get(i).getIsCorrect(), savedOptions.get(i).getIsCorrect());
            assertEquals(foundOptions.get(i).getQuestion(), savedOptions.get(i).getQuestion());
        }
    }

    @Test
    void create_happyPath() {
        //given
        OptionEntity toSave = generationOption();

        //when
        OptionEntity saved = optionRepository.save(toSave);
        OptionEntity found = optionRepository.findById(saved.getId())
                .orElseThrow(() -> new OptionNotFoundException(saved.getId()));

        //then
        assertNotNull(saved.getId());
        assertEquals(saved.getId(), found.getId());
    }

    @Test
    void update_happyPath() {
        //given
        OptionEntity saved = optionRepository.save(generationOption());

        OptionEntity found = optionRepository.findById(saved.getId())
                .orElseThrow(() -> new OptionNotFoundException(saved.getId()));

        //when
        saved.setText("Нет");
        optionRepository.save(saved);
        OptionEntity foundAfterUpdated = optionRepository.findById(saved.getId())
                .orElseThrow(() -> new OptionNotFoundException(saved.getId()));

        //then
        assertEquals(found.getId(), foundAfterUpdated.getId());
        assertEquals(found.getIsCorrect(), foundAfterUpdated.getIsCorrect());
        assertNotEquals(found.getText(), foundAfterUpdated.getText());
    }

    @Test
    void delete_happyPath() {
        //given
        OptionEntity saved = optionRepository.save(generationOption());

        //when
        optionRepository.deleteById(saved.getId());
        OptionEntity found = optionRepository.findById(saved.getId()).orElse(new OptionEntity());

        //then
        assertNull(found.getId());
        assertNull(found.getQuestion());
        assertNull(found.getText());
        assertNull(found.getIsCorrect());
    }

}
