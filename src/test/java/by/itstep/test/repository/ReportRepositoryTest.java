package by.itstep.test.repository;

import by.itstep.test.entity.QuestionReportEntity;
import by.itstep.test.entity.enums.ReportStatus;
import by.itstep.test.exception.ReportNotFoundException;
import by.itstep.test.integration.AbstractIntegrationTest;
import by.itstep.test.util.EntityGeneratorUtils;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Collectors;

import static by.itstep.test.util.EntityGeneratorUtils.generateListReports;
import static org.junit.jupiter.api.Assertions.*;

public class ReportRepositoryTest extends AbstractIntegrationTest {

    @Test
    void findById_happyPath() {
        //given
        QuestionReportEntity report = EntityGeneratorUtils.generateReportEntity();
        questionRepository.save(report.getQuestion());

        QuestionReportEntity savedReport = questionReportRepository.save(report);

        //when
        QuestionReportEntity foundReport = questionReportRepository.findById(savedReport.getId())
                .orElseThrow(() -> new ReportNotFoundException(savedReport.getId()));

        //then
        assertNotNull(foundReport);
        assertEquals(savedReport.getId(), foundReport.getId());
        assertEquals(savedReport.getQuestion().getId(), foundReport.getQuestion().getId());
        assertEquals(savedReport.getAuthorId(), foundReport.getAuthorId());
        assertEquals(savedReport.getReportStatus(), foundReport.getReportStatus());

    }

    @Test
    void findAll_happyPath() {
        //given
        List<QuestionReportEntity> reports = generateListReports();
        questionRepository.saveAll(reports.stream()
                .map(QuestionReportEntity::getQuestion)
                .collect(Collectors.toList()));

        List<QuestionReportEntity> savedReports = questionReportRepository.saveAll(reports);

        //when
        List<QuestionReportEntity> foundReports = questionReportRepository.findAll();

        //then
        for (int i = 0; i < foundReports.size(); i++) {
            assertEquals(savedReports.get(i).getId(), foundReports.get(i).getId());
            assertEquals(savedReports.get(i).getQuestion().getId(), foundReports.get(i).getQuestion().getId());
            assertEquals(savedReports.get(i).getAuthorId(), foundReports.get(i).getAuthorId());
            assertEquals(savedReports.get(i).getReportStatus(), foundReports.get(i).getReportStatus());
        }
    }

    @Test
    void create_happyPath() {
        //given
        QuestionReportEntity report = EntityGeneratorUtils.generateReportEntity();
        questionRepository.save(report.getQuestion());

        //when
        QuestionReportEntity savedReport = questionReportRepository.save(report);

        //then
        assertNotNull(savedReport.getId());
        assertEquals(report.getQuestion().getId(), savedReport.getQuestion().getId());
        assertEquals(report.getAuthorId(), savedReport.getAuthorId());
        assertEquals(report.getText(), savedReport.getText());
        assertEquals(report.getReportStatus(), savedReport.getReportStatus());

    }

    @Test
    void update_happyPath() {
        //given
        QuestionReportEntity report = EntityGeneratorUtils.generateReportEntity();
        questionRepository.save(report.getQuestion());
        report.setReportStatus(ReportStatus.OPEN);

        QuestionReportEntity reportBeforeUpdate = questionReportRepository.save(report);
        QuestionReportEntity foundReportBeforeUpdate = questionReportRepository.findById(reportBeforeUpdate.getId())
                .orElseThrow(() -> new ReportNotFoundException(reportBeforeUpdate.getId()));

        report.setReportStatus(ReportStatus.CLOSED);

        //when
        QuestionReportEntity reportAfterUpdate = questionReportRepository.save(report);

        QuestionReportEntity foundReportAfterUpdate = questionReportRepository.findById(reportAfterUpdate.getId())
                .orElseThrow(() -> new ReportNotFoundException(reportAfterUpdate.getId()));

        //then
        assertNotNull(reportAfterUpdate);
        assertEquals(reportBeforeUpdate.getId(), reportAfterUpdate.getId());
        assertNotEquals(foundReportBeforeUpdate.getReportStatus(), foundReportAfterUpdate.getReportStatus());
    }

    @Test
    void deleteById_happyPath() {
        //given
        QuestionReportEntity report = EntityGeneratorUtils.generateReportEntity();
        questionRepository.save(report.getQuestion());

        QuestionReportEntity savedReport = questionReportRepository.save(report);

        //when
        questionReportRepository.deleteById(savedReport.getId());

        //then
        assertThrows(ReportNotFoundException.class,
                () -> questionReportRepository.findById(savedReport.getId())
                                        .orElseThrow(() -> new ReportNotFoundException(savedReport.getId())));
    }

}
