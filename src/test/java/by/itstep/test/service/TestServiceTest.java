package by.itstep.test.service;

import by.itstep.test.dto.test.TestCreateDto;
import by.itstep.test.dto.test.TestFullDto;
import by.itstep.test.dto.test.TestPreviewDto;
import by.itstep.test.dto.test.TestUpdateDto;
import by.itstep.test.entity.QuestionEntity;
import by.itstep.test.entity.ResultEntity;
import by.itstep.test.entity.TestEntity;
import by.itstep.test.entity.enums.Action;
import by.itstep.test.exception.NumberQuestionException;
import by.itstep.test.exception.TestNotFoundException;
import by.itstep.test.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.data.domain.Page;

import java.util.List;

import static by.itstep.test.util.DtoGeneratorUtils.generateTestCreateDto;
import static by.itstep.test.util.DtoGeneratorUtils.generateTestUpdateDto;
import static by.itstep.test.util.EntityGeneratorUtils.*;
import static java.util.stream.Collectors.toList;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class TestServiceTest extends AbstractIntegrationTest {

    @Mock
    protected StepAchievementsClient achievementsClient;

    @Test
    void findById_happyPath() {
        //given
        TestEntity savedTest = testRepository.save(generateTest());
        ResultEntity resultEntity = generateResult();
        resultEntity.setTest(savedTest);
        resultRepository.save(resultEntity);

        //when
        TestFullDto foundDto = testService.findById(savedTest.getId());

        //then
        assertNotNull(foundDto);
        assertEquals(foundDto.getName(), savedTest.getName());
        assertEquals(foundDto.getDirection(), savedTest.getDirection());
        assertEquals(foundDto.getAuthorId(), savedTest.getAuthorId());
        assertEquals(foundDto.getLanguage(), savedTest.getLanguage());
        assertEquals(foundDto.getLevel(), savedTest.getLevel());
        assertEquals(foundDto.getRating(), savedTest.getRating());
        assertEquals(foundDto.getStatus(), savedTest.getStatus());
        assertEquals(foundDto.getAverageScore(), testRepository.getAverageScore(savedTest.getId()).orElse(0D));
        assertEquals(foundDto.getNumberOfPassed(), testRepository.getNumberPassed(savedTest.getId()).orElse(0));
    }

    @Test
    void findAll_happyPath() {
        //given
        List<QuestionEntity> questions = questionRepository.saveAll(generateListQuestion());
        List<TestEntity> savedTest = generateListTest();
        savedTest.forEach(testEntity -> testEntity.setQuestions(questions));
        testRepository.saveAll(savedTest);

        //when
        Page<TestPreviewDto> foundTests = testService.findAll(0, 10);
        List<TestPreviewDto> tests = foundTests.getContent();

        //then
        assertEquals(tests.size(), savedTest.size());
        for (int i = 0; i < tests.size(); i++) {
            assertEquals(tests.get(i).getNumberOfQuestions(), savedTest.get(i).getQuestions().size());
            assertEquals(tests.get(i).getId(), savedTest.get(i).getId());
            assertEquals(tests.get(i).getDirection(), savedTest.get(i).getDirection());
            assertEquals(tests.get(i).getName(), savedTest.get(i).getName());
            assertEquals(tests.get(i).getLanguage(), savedTest.get(i).getLanguage());
            assertEquals(tests.get(i).getRating(), savedTest.get(i).getRating());
            assertEquals(tests.get(i).getAuthorId(), savedTest.get(i).getAuthorId());
            assertEquals(tests.get(i).getLevel(), savedTest.get(i).getLevel());
            assertEquals(tests.get(i).getAverageScore(), testRepository.getAverageScore(savedTest.get(i).getId()).orElse(0D));
            assertEquals(tests.get(i).getNumberOfPassed(), testRepository.getNumberPassed(savedTest.get(i).getId()).orElse(0));
        }
    }

    @Test
    void create_happyPath() {
        //given
        TestCreateDto testToSave = generateTestCreateDto();
        testToSave.setQuestionIds(getListQuestionId());
        when(achievementsClient.recordAchievement(any(Action.class), any(Long.class)))
                .thenReturn(playerProgressFullDto);

        //when
        TestFullDto saved = testService.create(testToSave);
        TestFullDto foundSavedTest = testService.findById(saved.getId());

        //then
        assertNotNull(saved.getId());
        assertNotNull(foundSavedTest);
        assertEquals(saved.getId(), foundSavedTest.getId());
        assertEquals(foundSavedTest.getQuestions().size(), testToSave.getQuestionIds().size());
        assertEquals(foundSavedTest.getDirection(), saved.getDirection());
        assertEquals(foundSavedTest.getName(), saved.getName());
        assertEquals(foundSavedTest.getLanguage(), saved.getLanguage());
        assertEquals(foundSavedTest.getRating(), saved.getRating());
        assertEquals(foundSavedTest.getLevel(), saved.getLevel());
        assertEquals(foundSavedTest.getStatus(), saved.getStatus());
        assertEquals(foundSavedTest.getAverageScore(), testRepository.getAverageScore(saved.getId()).orElse(0D));
        assertEquals(foundSavedTest.getNumberOfPassed(), testRepository.getNumberPassed(saved.getId()).orElse(0));
    }

    @Test
    void update_happyPath() {
        //given
        TestCreateDto testToCreate = generateTestCreateDto();
        testToCreate.setQuestionIds(getListQuestionId());

        TestFullDto savedTest = testService.create(testToCreate);
        ResultEntity resultEntity = generateResult();
        resultEntity.setTest(testRepository.findById(savedTest.getId())
                .orElseThrow(() -> new TestNotFoundException(savedTest.getId())));
        resultRepository.save(resultEntity);

        TestFullDto foundTest = testService.findById(savedTest.getId());

        TestUpdateDto testToUpdate = generateTestUpdateDto();
        testToUpdate.setId(savedTest.getId());
        List<Long> listQuestionForUpdate = List.of(testToCreate.getQuestionIds().get(0),
                testToCreate.getQuestionIds().get(1));
        testToUpdate.setQuestionIds(listQuestionForUpdate);

        //when
        TestFullDto testUpdated = testService.update(testToUpdate);

        //then
        assertNotNull(testUpdated);
        assertEquals(foundTest.getId(), testUpdated.getId());
        assertNotEquals(foundTest.getName(), testUpdated.getName());
        assertEquals(foundTest.getLevel(), testUpdated.getLevel());
        assertNotEquals(foundTest.getRating(), testUpdated.getRating());
        assertEquals(foundTest.getAuthorId(), testUpdated.getAuthorId());
        assertEquals(foundTest.getLanguage(), testUpdated.getLanguage());
        assertNotEquals(foundTest.getDirection(), testUpdated.getDirection());
        assertNotEquals(foundTest.getStatus(), testUpdated.getStatus());
        assertNotEquals(foundTest.getQuestions().size(), testUpdated.getQuestions().size());
        assertEquals(foundTest.getAverageScore(), testRepository.getAverageScore(testUpdated.getId()).orElse(0D));
        assertEquals(foundTest.getNumberOfPassed(), testRepository.getNumberPassed(testUpdated.getId()).orElse(0));

    }

    @Test
    void deleteById_happyPath() {
        //given
        TestFullDto testSaved = testService.create(generateTestCreateDto());

        //when
        testService.deleteById(testSaved.getId());

        Exception exception = assertThrows(TestNotFoundException.class,
                () -> testService.findById(testSaved.getId()));

        //then
        assertTrue(exception.getMessage().contains(String.valueOf(testSaved.getId())));
    }

    @Test
    void testUpdate_whenNotNull() {
        //given
        TestUpdateDto testUpdateDto = generateTestUpdateDto();
        testUpdateDto.setId(-1L);

        //when
        Exception exception = assertThrows(TestNotFoundException.class,
                () -> testService.update(testUpdateDto));

        //then
        assertTrue(exception.getMessage().contains(String.valueOf(testUpdateDto.getId())));
    }

    @Test
    void testFindById_whenNotNull() {
        //given
        Long notExistingId = 10L;

        //when
        Exception exception = assertThrows(TestNotFoundException.class,
                () -> testService.findById(notExistingId));

        //then
        assertTrue(exception.getMessage().contains(String.valueOf(notExistingId)));
    }

    @Test
    void testDeleteById_whenNotNull() {
        //given
        Long notExistingId = 500L;

        //when
        Exception exception = assertThrows(TestNotFoundException.class,
                () -> testService.deleteById(notExistingId));

        //then
        assertTrue(exception.getMessage().contains(String.valueOf(notExistingId)));
    }

    @Test
    void testCreate_whenInvalideNumberQuestion() {
        //given
        TestCreateDto testToSave = generateTestCreateDto();
        testToSave.setQuestionIds(List.of(1L, 2L, 3L));

        //when
        Exception exception = assertThrows(NumberQuestionException.class,
                () -> testService.create(testToSave));

        //then
        assertTrue(exception.getMessage().contains("Wrong number of questions."));
    }

    private List<Long> getListQuestionId() {
        List<QuestionEntity> questions = generateListQuestion();
        List<QuestionEntity> savedQuestions = questionRepository.saveAll(questions);

        return savedQuestions.stream()
                .map(QuestionEntity::getId)
                .collect(toList());
    }
}
