package by.itstep.test.service;

import by.itstep.test.dto.option.OptionCreateDto;
import by.itstep.test.dto.question.QuestionCreateDto;
import by.itstep.test.dto.question.QuestionFullDto;
import by.itstep.test.dto.question.QuestionPreviewDto;
import by.itstep.test.dto.question.QuestionUpdateDto;
import by.itstep.test.entity.QuestionEntity;
import by.itstep.test.exception.CountCorrectOptionException;
import by.itstep.test.exception.QuestionNotFoundException;
import by.itstep.test.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.Page;

import java.util.List;

import static by.itstep.test.util.DtoGeneratorUtils.generateQuestionCreateDto;
import static by.itstep.test.util.DtoGeneratorUtils.generateQuestionUpdateDto;
import static by.itstep.test.util.EntityGeneratorUtils.generateListQuestion;
import static by.itstep.test.util.EntityGeneratorUtils.generateQuestion;
import static org.junit.jupiter.api.Assertions.*;

public class QuestionServiceTest extends AbstractIntegrationTest {

    @Test
    void findById_happyPath() {
        //given
        QuestionEntity saved = questionRepository.save(generateQuestion());

        //when
        QuestionFullDto foundQuestion = questionService.findById(saved.getId());

        //then
        assertNotNull(foundQuestion);
        assertEquals(foundQuestion.getId(), saved.getId());
        assertEquals(foundQuestion.getStatus(), saved.getStatus());
        assertEquals(foundQuestion.getText(), saved.getText());
        assertEquals(foundQuestion.getAuthorId(), saved.getAuthorId());
        assertEquals(foundQuestion.getLanguage(), saved.getLanguage());
        assertEquals(foundQuestion.getType(), saved.getType());
        assertEquals(foundQuestion.getLevel(), saved.getLevel());
        assertEquals(foundQuestion.getCodeBlock(), saved.getCodeBlock());
    }

    @Test
    void findAll_happyPath() {
        //given
        List<QuestionEntity> saved = questionRepository.saveAll(generateListQuestion());

        //when
        Page<QuestionPreviewDto> foundQuestion = questionService.findAll(0 ,10);
        List<QuestionPreviewDto> questions = foundQuestion.getContent();

        //then
        assertNotNull(foundQuestion);
        assertEquals(foundQuestion.getTotalElements(), saved.size());
        for (int i = 0; i < questions.size(); i++) {
            assertEquals(questions.get(i).getId(), saved.get(i).getId());
            assertEquals(questions.get(i).getText(), saved.get(i).getText());
            assertEquals(questions.get(i).getLanguage(), saved.get(i).getLanguage());
            assertEquals(questions.get(i).getType(), saved.get(i).getType());
            assertEquals(questions.get(i).getLevel(), saved.get(i).getLevel());
        }
    }

    @Test
    void create_happyPath() {
        //given
        QuestionCreateDto questionToSave = generateQuestionCreateDto();

        //when
        QuestionFullDto saved = questionService.create(questionToSave);
        QuestionFullDto foundSavedQuestion = questionService.findById(saved.getId());

        //then
        assertNotNull(saved.getId());
        assertNotNull(foundSavedQuestion);
        assertEquals(saved.getId(), foundSavedQuestion.getId());
        assertEquals(saved.getOptions().size(), foundSavedQuestion.getOptions().size());
        for (int i = 0; i < foundSavedQuestion.getOptions().size(); i++) {
            assertEquals(saved.getOptions().get(i).getId(), foundSavedQuestion.getOptions().get(i).getId());
            assertEquals(saved.getOptions().get(i).getText(), foundSavedQuestion.getOptions().get(i).getText());
            assertEquals(saved.getOptions().get(i).getIsCorrect(), foundSavedQuestion.getOptions().get(i).getIsCorrect());
        }
        assertEquals(saved.getAuthorId(), foundSavedQuestion.getAuthorId());
        assertEquals(saved.getText(), foundSavedQuestion.getText());
        assertEquals(saved.getStatus(), foundSavedQuestion.getStatus());
        assertEquals(foundSavedQuestion.getLanguage(), saved.getLanguage());
        assertEquals(foundSavedQuestion.getType(), saved.getType());
        assertEquals(foundSavedQuestion.getLevel(), saved.getLevel());
    }

    @Test
    void update_happyPath() {
        //given
        QuestionFullDto savedQuestion = questionService.create(generateQuestionCreateDto());

        QuestionUpdateDto questionUpdateDto = generateQuestionUpdateDto();
        questionUpdateDto.setId(savedQuestion.getId());

        //when
        QuestionFullDto questionUpdated = questionService.update(questionUpdateDto);

        //then
        assertNotNull(questionUpdated);
        assertNotEquals(questionUpdated.getAuthorId(), savedQuestion.getAuthorId());
        assertNotEquals(questionUpdated.getStatus(), savedQuestion.getStatus());
        assertNotEquals(questionUpdated.getText(), savedQuestion.getText());
        assertNotEquals(questionUpdated.getOptions().size(), savedQuestion.getOptions().size());
        assertNotEquals(questionUpdated.getLanguage(), savedQuestion.getLanguage());
        assertEquals(questionUpdated.getType(), savedQuestion.getType());
        assertNotEquals(questionUpdated.getLevel(), savedQuestion.getLevel());
    }

    @Test
    void deleteById_happyPath() {
        //given
        QuestionFullDto savedQuestion = questionService.create(generateQuestionCreateDto());

        //when
        questionService.deleteById(savedQuestion.getId());

        Exception exception = assertThrows(QuestionNotFoundException.class,
                () -> questionService.findById(savedQuestion.getId()));
        //then
        assertTrue(exception.getMessage().contains(String.valueOf(savedQuestion.getId())));
    }

    @Test
    void testUpdate_whenNotNull() {
        //given
        QuestionUpdateDto questionUpdateDto = generateQuestionUpdateDto();

        Long notExistingId = -1L;
        questionUpdateDto.setId(notExistingId);

        //when
        Exception exception = assertThrows(QuestionNotFoundException.class,
                () -> questionService.update(questionUpdateDto));

        //then
        assertTrue(exception.getMessage().contains(String.valueOf(notExistingId)));
    }

    @Test
    void testFindById_whenNotNull() {
        //given
        Long notExistingId = 10L;

        //when
        Exception exception = assertThrows(QuestionNotFoundException.class,
                () -> questionService.findById(notExistingId));

        //then
        assertTrue(exception.getMessage().contains(String.valueOf(notExistingId)));
    }

    @Test
    void testDeleteById_whenNotNull() {
        //given
        Long notExistingId = 500L;

        //when
        Exception exception = assertThrows(QuestionNotFoundException.class,
                () -> questionService.deleteById(notExistingId));

        //then
        assertTrue(exception.getMessage().contains(String.valueOf(notExistingId)));
    }

    @Test
    void testCreate_whenInvalideCountCorrectOption() {
        //given
        QuestionCreateDto questionToSave = generateQuestionCreateDto();
        for (OptionCreateDto optionCreateDto : questionToSave.getOptions()) {
            optionCreateDto.setIsCorrect(true);
        }

        //when
        Exception exception = assertThrows(CountCorrectOptionException.class,
                () -> questionService.create(questionToSave));

        //then
        assertEquals(exception.getMessage(), "One Correct Option Needed.");
    }
}
