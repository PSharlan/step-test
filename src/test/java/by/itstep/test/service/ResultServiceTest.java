package by.itstep.test.service;

import by.itstep.test.dto.result.ResultCreateDto;
import by.itstep.test.dto.result.ResultFullDto;
import by.itstep.test.dto.result.ResultPreviewDto;
import by.itstep.test.entity.OptionEntity;
import by.itstep.test.entity.QuestionEntity;
import by.itstep.test.entity.ResultEntity;
import by.itstep.test.entity.TestEntity;
import by.itstep.test.entity.enums.Action;
import by.itstep.test.exception.OptionNotFoundException;
import by.itstep.test.exception.QuestionNotFoundException;
import by.itstep.test.exception.ResultNotFoundException;
import by.itstep.test.exception.TestNotFoundException;
import by.itstep.test.integration.AbstractIntegrationTest;
import by.itstep.test.service.impl.ResultServiceImpl;
import by.itstep.test.service.impl.TestServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static by.itstep.test.util.DtoGeneratorUtils.generateResultCreateDto;
import static by.itstep.test.util.EntityGeneratorUtils.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class ResultServiceTest extends AbstractIntegrationTest {

    @Test
    void findById_happyPath() {
        //given
        ResultEntity saved = resultRepository.save(generateResult());

        //when
        ResultFullDto foundResult = resultService.findById(saved.getId());

        //then
        assertNotNull(foundResult);
        assertEquals(foundResult.getId(), saved.getId());
        assertEquals(foundResult.getTimeSpent(), saved.getTimeSpent());
    }

    @Test
    void findByUserIdAndTestId_happyPath() {
        //given
        when(achievementsClient.recordAchievement(any(Action.class), any(Long.class)))
                .thenReturn(playerProgressFullDto);
        ResultFullDto result = resultService.create(getResultCreateDto());
        Long userId = result.getUserId();
        Long testId = result.getTestPreview().getId();


        //when
        List<ResultPreviewDto> foundResults = resultService
                .findByUserIdAndTestId(userId, testId);

        //then
        assertEquals(foundResults.get(0).getTestPreview().getId(), result.getTestPreview().getId());
        assertEquals(foundResults.get(0).getUserId(), result.getUserId());
    }

    @Test
    void findByUserIdAndTestId_whenTestIdAndUserIdNotFound() {
        //given
        Long existingUserId = 10000000L;
        Long existingTestId = 10000000L;

        //when

        //then
        assertThrows(TestNotFoundException.class,
                () -> resultService.findByUserIdAndTestId(existingUserId, existingTestId));
    }

    @Test
    void findAll_happyPath() {
        //given
        List<ResultEntity> resultsSaved = resultRepository.saveAll(generateListResults());

        //when
        Page<ResultPreviewDto> foundResults = resultService.findAll(0, 10);
        List<ResultPreviewDto> results = foundResults.getContent();

        //then
        assertNotNull(foundResults);
        assertEquals(results.size(), resultsSaved.size());
        for (int i = 0; i < results.size(); i++) {
            assertEquals(results.get(i).getId(), resultsSaved.get(i).getId());
            assertEquals(results.get(i).getScore(), resultsSaved.get(i).getScore());
            assertEquals(results.get(i).getUserId(), resultsSaved.get(i).getUserId());
            assertEquals(results.get(i).getTimeSpent(), resultsSaved.get(i).getTimeSpent());
        }
    }

    @Test
    void create_happyPath() {
        //given
        when(achievementsClient.recordAchievement(any(Action.class), any(Long.class)))
                .thenReturn(playerProgressFullDto);
//        when(restTemplate.postForEntity(any(String.class),
//                new HttpEntity<>(new PlayerCommittedActionDto(any(Action.class), any(Long.class))),
//                PlayerProgressFullDto.class)).thenReturn(null);

        //when
        ResultFullDto savedResult = resultService.create(getResultCreateDto());
        ResultFullDto foundResult = resultService.findById(savedResult.getId());

        //then
        assertNotNull(savedResult);
        assertNotNull(savedResult.getId());
        assertEquals(savedResult.getId(), foundResult.getId());
        assertEquals(savedResult.getTimeSpent(), foundResult.getTimeSpent());
    }

    @Test
    void deleteById_happyPath() {
        //given
        ResultEntity resultToSave = resultRepository.save(generateResult());

        //when
        resultService.deleteById(resultToSave.getId());
        Exception exception = assertThrows(ResultNotFoundException.class,
                () -> resultService.findById(resultToSave.getId()));

        //then
        assertTrue(exception.getMessage().contains(String.valueOf(resultToSave.getId())));
    }

    @Test
    void testFindById_whenNotNull() {
        //given
        Long notExistingId = 0L;

        //when
        Exception exception = assertThrows(ResultNotFoundException.class,
                () -> resultService.findById(notExistingId));

        //then
        assertTrue(exception.getMessage().contains(String.valueOf(notExistingId)));
    }

    @Test
    void deleteById_whenNotFound() {
        //given
        Long notExistingId = -1L;

        //when
        Exception exception = assertThrows(ResultNotFoundException.class,
                () -> resultService.deleteById(notExistingId));

        //then
        assertTrue(exception.getMessage().contains(String.valueOf(notExistingId)));
    }

    @Test
    void findById_whenNotFound() {
        //given
        Long notExistingId = -1L;

        Exception exception = assertThrows(ResultNotFoundException.class,
                () -> resultService.findById(notExistingId));

        //then
        assertTrue(exception.getMessage().contains(String.valueOf(notExistingId)));
    }

    @Test
    void createById_whenTestNotFound() {
        //given
        ResultCreateDto resultCreateDto = getResultCreateDto();
        resultCreateDto.setTestId(-1L);

        //when
        Exception exception = assertThrows(TestNotFoundException.class,
                () -> resultService.create(resultCreateDto));

        //then
        assertTrue(exception.getMessage().contains(String.valueOf(resultCreateDto.getTestId())));
    }

    @Test
    void createById_whenQuestionNotFound() {
        //given
        ResultCreateDto resultCreateDto = getResultCreateDto();
        resultCreateDto.getAnswers().get(0).setQuestionId(-1L);

        //when
        Exception exception = assertThrows(QuestionNotFoundException.class,
                () -> resultService.create(resultCreateDto));

        //then
        assertTrue(exception.getMessage().contains(
                String.valueOf(resultCreateDto.getAnswers().get(0).getQuestionId())));
    }

    @Test
    void createById_whenOptionNotFound() {
        //given
        ResultCreateDto resultCreateDto = getResultCreateDto();
        resultCreateDto.getAnswers().get(0).setOptionId(-1L);

        //when
        Exception exception = assertThrows(OptionNotFoundException.class,
                () -> resultService.create(resultCreateDto));

        //then
        assertTrue(exception.getMessage().contains(
                String.valueOf(resultCreateDto.getAnswers().get(0).getOptionId())));
    }

    private ResultCreateDto getResultCreateDto() {
        TestEntity testEntity = generateTest();

        List<QuestionEntity> questions = generateListQuestion();
        for (QuestionEntity question : questions) {
            List<OptionEntity> options = generateListOption();

            question.setOptions(options);
            for (OptionEntity option : options) {
                option.setQuestion(question);
            }
        }
        testEntity.setQuestions(questions);
        questions.forEach(q -> q.setTests(List.of(testEntity)));

        List<QuestionEntity> savedQuestion = questionRepository.saveAll(questions);
        TestEntity test = testRepository.save(testEntity);

        return generateResultCreateDto(savedQuestion, test);
    }
}
