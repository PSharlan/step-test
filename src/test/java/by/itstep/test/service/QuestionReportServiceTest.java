package by.itstep.test.service;

import by.itstep.test.dto.report.ReportCreateDto;
import by.itstep.test.dto.report.ReportFullDto;
import by.itstep.test.dto.report.ReportPreviewDto;
import by.itstep.test.dto.report.ReportUpdateDto;
import by.itstep.test.entity.QuestionReportEntity;
import by.itstep.test.entity.enums.ReportStatus;
import by.itstep.test.exception.QuestionNotFoundException;
import by.itstep.test.exception.ReportNotFoundException;
import by.itstep.test.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.stream.Collectors;

import static by.itstep.test.util.DtoGeneratorUtils.generateReportCreateDto;
import static by.itstep.test.util.EntityGeneratorUtils.generateListReports;
import static by.itstep.test.util.EntityGeneratorUtils.generateQuestion;
import static org.junit.jupiter.api.Assertions.*;

public class QuestionReportServiceTest extends AbstractIntegrationTest {

    @Test
    void testFindById_happyPath() {
        //given
        ReportCreateDto reportToSave = generateReportCreateDto();
        reportToSave.setQuestionId(questionRepository.save(generateQuestion()).getId());

        ReportFullDto reportFullDto = reportService.create(reportToSave);

        //then
        ReportFullDto foundReport = reportService.findById(reportFullDto.getId());

        //when
        assertNotNull(foundReport);
        assertEquals(foundReport, reportFullDto);
    }

    @Test
    void testFindAll_happyPath() {
        //given
        List<QuestionReportEntity> reports = generateListReports();
        questionRepository.saveAll(reports.stream()
                .map(QuestionReportEntity::getQuestion)
                .collect(Collectors.toList()));

        List<QuestionReportEntity> savedReports = questionReportRepository.saveAll(reports);

        //then
        Page<ReportPreviewDto> foundReports = reportService.findAll(0, 10);
        List<ReportPreviewDto> foundRep = foundReports.getContent();

        //when
        assertNotNull(foundReports);
        assertEquals(foundReports.getNumberOfElements(), savedReports.size());
        for (int i = 0; i < savedReports.size(); i++) {
            assertEquals(foundRep.get(i).getId(), savedReports.get(i).getId());
            assertEquals(foundRep.get(i).getAuthorId(), savedReports.get(i).getAuthorId());
            assertEquals(foundRep.get(i).getText(), savedReports.get(i).getText());
            assertEquals(foundRep.get(i).getReportStatus(), savedReports.get(i).getReportStatus());
        }
    }

    @Test
    void create_happyPath() {
        //given
        ReportCreateDto reportToSave = generateReportCreateDto();
        reportToSave.setQuestionId(questionRepository.save(generateQuestion()).getId());

        //when
        ReportFullDto savedReport = reportService.create(reportToSave);

        //then
        assertNotNull(savedReport);
        assertNotNull(savedReport.getId());
        assertEquals(savedReport.getAuthorId(), reportToSave.getAuthorId());
        assertEquals(savedReport.getText(), reportToSave.getText());
    }

    @Test
    void update_happyPath() {
        //given
        ReportCreateDto reportToSave = generateReportCreateDto();
        reportToSave.setQuestionId(questionRepository.save(generateQuestion()).getId());

        ReportFullDto savedReport = reportService.create(reportToSave);

        ReportStatus status = savedReport.getReportStatus() != ReportStatus.CLOSED? ReportStatus.CLOSED : ReportStatus.OPEN;
        ReportUpdateDto reportToUpdate = ReportUpdateDto.builder()
                .id(savedReport.getId())
                .reportStatus(status)
                .text(savedReport.getText())
                .build();

        //when
        ReportFullDto updatedReport = reportService.update(reportToUpdate);

        //then
        assertEquals(updatedReport.getId(), savedReport.getId());
        assertEquals(updatedReport.getText(), savedReport.getText());
        assertNotEquals(updatedReport.getReportStatus(), savedReport.getReportStatus());
    }

    @Test
    void delete_happyPath() {
        //given
        ReportCreateDto reportToSave = generateReportCreateDto();
        reportToSave.setQuestionId(questionRepository.save(generateQuestion()).getId());

        ReportFullDto savedReport = reportService.create(reportToSave);

        //when
        reportService.deleteById(savedReport.getId());

        //then
        assertThrows(ReportNotFoundException.class,
                () -> reportService.findById(savedReport.getId()));
    }

    @Test
    void findById_whenNotNull() {
        //given
        Long notExistingId = 100000L;

        //when

        //then
        assertThrows(ReportNotFoundException.class,
                () -> reportService.findById(notExistingId));

    }

    @Test
    void update_whenNotNull() {
        //given
        ReportUpdateDto reportToUpdate = ReportUpdateDto.builder()
                .id(100000L)
                .reportStatus(ReportStatus.OPEN)
                .text("OLOL")
                .build();

        //when

        //then
        assertThrows(ReportNotFoundException.class,
                () -> reportService.update(reportToUpdate));
    }

    @Test
    void deleteById_whenNotNull() {
        //given
        Long notExistingId = 100000L;

        //when

        //then
        assertThrows(ReportNotFoundException.class,
                () -> reportService.deleteById(notExistingId));
    }

    @Test
    void create_whenQuestionNotFoundId() {
        //given
        ReportCreateDto report = generateReportCreateDto();
        report.setQuestionId(100000L);

        //when

        //then
        assertThrows(QuestionNotFoundException.class,
                () -> reportService.create(report));
    }
}
