package by.itstep.test.service;

import by.itstep.test.dto.test.TestCreateDto;
import by.itstep.test.dto.test.TestFullDto;
import by.itstep.test.dto.test.TestPreviewDto;
import by.itstep.test.dto.test.TestUpdateDto;
import by.itstep.test.entity.TestEntity;
import by.itstep.test.entity.enums.Direction;
import by.itstep.test.entity.enums.Language;
import by.itstep.test.entity.enums.Level;
import by.itstep.test.integration.AbstractIntegrationTest;
import by.itstep.test.mapper.TestMapper;
import by.itstep.test.repository.QuestionRepository;
import by.itstep.test.repository.TestRepository;
import by.itstep.test.service.impl.TestServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.*;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static by.itstep.test.mapper.TestMapper.TEST_MAPPER;
import static by.itstep.test.util.DtoGeneratorUtils.generateTestUpdateDto;
import static by.itstep.test.util.EntityGeneratorUtils.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

public class TestServiceWithMockitoTest extends AbstractIntegrationTest {

    @MockBean
    private TestRepository testRepository;

    @MockBean
    private TestMapper testMapper;

    @MockBean
    private QuestionRepository questionRepository;

    @Autowired
    private TestService testService;

    private TestEntity test;
    private List<TestEntity> tests;

    @BeforeEach
    void getTest() {
        test = generateTestFromMockito();
        tests = generateListTestFromMockito();
    }

    @Test
    void findById() {
        //given
        when(testRepository.findById(any(Long.class))).thenReturn(Optional.of(test));

        //when
        TestFullDto foundDto = testService.findById(test.getId());

        //then
        assertNotNull(foundDto);
        assertEquals(foundDto.getId(), test.getId());
    }

    @Test
    void findAll() {
        //given
        TestEntity testEntity = new TestEntity();
        testEntity.setId(1L);
        Page<TestEntity> pageTestEntity = new PageImpl<>(List.of(testEntity));

        TestPreviewDto testPreviewDto = new TestPreviewDto();
        testPreviewDto.setId(1L);


        when(testMapper.mapToPreviewDto(testEntity)).thenReturn(testPreviewDto);
        when(testRepository.findAll(any(PageRequest.class))).thenReturn(pageTestEntity);

        //when
        List<TestPreviewDto> foundTests = testService.findAll(0, 10).getContent();

        //then
        assertEquals(foundTests.size(), pageTestEntity.getContent().size());
    }

    @Test
    void create_happyPath() {
        //given
        when(testRepository.save(any(TestEntity.class))).thenReturn(test);

        //when
        TestFullDto saved = testService.create(new TestCreateDto());

        //then
        assertNotNull(saved.getId());
        assertEquals(saved.getId(), test.getId());
    }

    @Test
    void update_happyPath() {
        //given
        TestEntity testBeforeUpdate = generateTestFromMockito();
        when(testRepository.findById(any(Long.class))).thenReturn(Optional.of(test));
        when(testRepository.save(any(TestEntity.class))).then(returnsFirstArg());
        when(questionRepository.findAllById(anyIterable())).thenReturn(Collections.emptyList());

        TestUpdateDto testToUpdate = generateTestUpdateDto();
        testToUpdate.setId(1L);

        //when
        TestFullDto testUpdated = testService.update(testToUpdate);

        //then
        assertNotNull(testUpdated);
        assertEquals(testToUpdate.getId(), testBeforeUpdate.getId());
        assertNotEquals(testUpdated.getName(), testBeforeUpdate.getName());
    }
}
