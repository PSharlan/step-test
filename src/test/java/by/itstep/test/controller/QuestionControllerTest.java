package by.itstep.test.controller;

import by.itstep.test.dto.question.QuestionCreateDto;
import by.itstep.test.dto.question.QuestionFullDto;
import by.itstep.test.dto.question.QuestionUpdateDto;
import by.itstep.test.entity.QuestionEntity;
import by.itstep.test.integration.AbstractIntegrationTest;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;

import static by.itstep.test.util.DtoGeneratorUtils.generateQuestionCreateDto;
import static by.itstep.test.util.DtoGeneratorUtils.generateQuestionUpdateDto;
import static by.itstep.test.util.EntityGeneratorUtils.generateListQuestion;
import static by.itstep.test.util.EntityGeneratorUtils.generateQuestion;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class QuestionControllerTest extends AbstractIntegrationTest {

    @Test
    void testFindById_happyPath() throws Exception {
        //given
        QuestionEntity savedQuestion = questionRepository.save(generateQuestion());

        //when
        MvcResult result = mockMvc.perform(get("/questions/{id}", savedQuestion.getId()))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        QuestionFullDto foundQuestion = objectMapper.readValue(bytes, QuestionFullDto.class);

        //then
        assertNotNull(foundQuestion);
        assertNotNull(foundQuestion.getId());
        assertEquals(foundQuestion.getId(), savedQuestion.getId());
        assertEquals(foundQuestion.getText(), savedQuestion.getText());
        assertEquals(foundQuestion.getStatus(), savedQuestion.getStatus());
        assertEquals(foundQuestion.getAuthorId(), savedQuestion.getAuthorId());
    }

    @Test
    void testFindAll_happyPath() throws Exception {
        //given
        List<QuestionEntity> savedQuestions = questionRepository.saveAll(generateListQuestion());

        //when
        mockMvc.perform(get("/questions?page=0&size=10"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content",hasSize(savedQuestions.size())))
                .andReturn();

        //then
    }

    @Test
    void testFindAll_whenPageNullAndSizeNull() throws Exception {
        //given
        List<QuestionEntity> savedQuestions = questionRepository.saveAll(generateListQuestion());

        //when
        mockMvc.perform(get("/questions"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content",hasSize(savedQuestions.size())))
                .andReturn();

        //then
    }

    @Test
    void testCreate_happyPath() throws Exception {
        //given
        QuestionEntity questionToSave = generateQuestion();

        //when
        MvcResult result = mockMvc.perform(post("/questions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(questionToSave)))
                .andExpect(status().isCreated())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        QuestionFullDto savedQuestion = objectMapper.readValue(bytes, QuestionFullDto.class);

        //then
        assertNotNull(savedQuestion);
        assertNotNull(savedQuestion.getId());
        assertEquals(savedQuestion.getAuthorId(), questionToSave.getAuthorId());
        assertEquals(savedQuestion.getText(), questionToSave.getText());
        assertEquals(savedQuestion.getStatus(), questionToSave.getStatus());
    }

    @Test
    void testUpdate_happyPath() throws Exception {
        //given
        QuestionEntity savedQuestion = questionRepository.save(generateQuestion());

        QuestionUpdateDto updateQuestion = generateQuestionUpdateDto();
        updateQuestion.setId(savedQuestion.getId());

        //when
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.put("/questions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateQuestion)))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        QuestionUpdateDto updatedQuestion = objectMapper.readValue(bytes, QuestionUpdateDto.class);

        //then
        assertNotNull(updatedQuestion);
        assertNotNull(updatedQuestion.getId());
        assertEquals(updatedQuestion.getId(), savedQuestion.getId());
        assertEquals(updatedQuestion.getAuthorId(), savedQuestion.getAuthorId());
        assertNotEquals(updatedQuestion.getText(), savedQuestion.getText());
        assertNotEquals(updatedQuestion.getStatus(), savedQuestion.getStatus());
    }

    @Test
    void testDelete_happyPath() throws Exception {
        //given
        QuestionEntity savedQuestion = questionRepository.save(generateQuestion());

        //when
        mockMvc.perform(delete("/questions/{id}", savedQuestion.getId()))
                .andExpect(status().isOk());

        //then/
        mockMvc.perform(get("/questions/{id}", savedQuestion.getId()))
                .andExpect(status().isNotFound());
    }

    @Test
    void testFindById_whenNotFound() throws Exception {
        //given
        Long notExistingId = 100000L;

        //when
        mockMvc.perform(get("/questions/{id}", notExistingId))
                .andExpect(status().isNotFound());

        //then
    }

    @Test
    void testDelete_whenNotFound() throws Exception {
        //given
        Long notExistingId = 100000L;

        //when
        mockMvc.perform(delete("/questions/{id}", notExistingId))
                .andExpect(status().isNotFound());

        //then

    }

    @Test
    void testUpdate_whenNotFound() throws Exception {
        //given
        QuestionUpdateDto updateDto = generateQuestionUpdateDto();
        updateDto.setId(100000L);

        //when
        mockMvc.perform(put("/questions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isNotFound());

        //then

    }

    @Test
    void testCreate_whenInValidCountCorrect() throws Exception {
        //given
        QuestionCreateDto createDto = generateQuestionCreateDto();
        createDto.getOptions().get(0).setIsCorrect(true);

        //when
        mockMvc.perform(post("/questions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isBadRequest());

        //then
    }

    @Test
    void testUpdate_whenInValidCountCorrect() throws Exception {
        //given
        QuestionEntity savedQuestion = questionRepository.save(generateQuestion());
        QuestionUpdateDto updateDto = generateQuestionUpdateDto();
        updateDto.setId(savedQuestion.getId());
        updateDto.getOptions().get(0).setIsCorrect(true);

        //when
        mockMvc.perform(put("/questions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isBadRequest());

        //then
    }

    @Test
    void testCreate_whenTextEmpty() throws Exception {
        //given
        QuestionCreateDto questionCreateDto = generateQuestionCreateDto();
        questionCreateDto.setText("");

        //when
        mockMvc.perform(post("/questions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(questionCreateDto)))
                .andExpect(status().isBadRequest());

        //then
    }

    @Test
    void testCreate_whenAuthorIdNull() throws Exception {
        //given
        QuestionCreateDto questionCreateDto = generateQuestionCreateDto();
        questionCreateDto.setAuthorId(null);

        //when
        mockMvc.perform(post("/questions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(questionCreateDto)))
                .andExpect(status().isBadRequest());

        //then
    }

    @Test
    void testUpdate_whenTextEmpty() throws Exception {
        //given
        QuestionEntity savedQuestion = questionRepository.save(generateQuestion());

        QuestionUpdateDto questionUpdateDto = generateQuestionUpdateDto();
        questionUpdateDto.setId(savedQuestion.getId());
        questionUpdateDto.setText("");

        //when
        mockMvc.perform(put("/questions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(questionUpdateDto)))
                .andExpect(status().isBadRequest());

        //then
    }

    @Test
    void testUpdate_whenAuthorIdNull() throws Exception {
        //given
        QuestionEntity savedQuestion = questionRepository.save(generateQuestion());

        QuestionUpdateDto questionUpdateDto = generateQuestionUpdateDto();
        questionUpdateDto.setId(savedQuestion.getId());
        questionUpdateDto.setAuthorId(null);

        //when
        mockMvc.perform(put("/questions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(questionUpdateDto)))
                .andExpect(status().isBadRequest());

        //then
    }
}
