package by.itstep.test.controller;

import by.itstep.test.dto.report.ReportCreateDto;
import by.itstep.test.dto.report.ReportFullDto;
import by.itstep.test.dto.report.ReportUpdateDto;
import by.itstep.test.entity.QuestionEntity;
import by.itstep.test.entity.QuestionReportEntity;
import by.itstep.test.entity.enums.ReportStatus;
import by.itstep.test.integration.AbstractIntegrationTest;
import com.fasterxml.jackson.core.type.TypeReference;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;

import java.util.List;
import java.util.stream.Collectors;

import static by.itstep.test.util.DtoGeneratorUtils.generateReportCreateDto;
import static by.itstep.test.util.EntityGeneratorUtils.*;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class QuestionReportControllerTest extends AbstractIntegrationTest {

    @Test
    void findById_happyPath() throws Exception {
        //given
        QuestionReportEntity reportToSave = generateReportEntity();
        questionRepository.save(reportToSave.getQuestion());

        QuestionReportEntity savedReport = questionReportRepository.save(reportToSave);

        //when
        MvcResult result = mockMvc.perform(get("/reports/{id}", savedReport.getId()))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        ReportFullDto foundReport = objectMapper.readValue(bytes, new TypeReference<>() {
        });

        //then
        assertNotNull(foundReport);
        assertEquals(foundReport.getId(), savedReport.getId());
        assertEquals(foundReport.getAuthorId(), savedReport.getAuthorId());
        assertEquals(foundReport.getText(), savedReport.getText());
        assertEquals(foundReport.getReportStatus(), savedReport.getReportStatus());
    }

    @Test
    void findAll_whenPageAndSizeNull() throws Exception {
        //given
        List<QuestionReportEntity> reports = generateListReports();
        questionRepository.saveAll(reports.stream()
                .map(QuestionReportEntity::getQuestion)
                .collect(Collectors.toList()));

        List<QuestionReportEntity> savedReports = questionReportRepository.saveAll(reports);

        //when
        MvcResult result = mockMvc.perform(get("/reports"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", hasSize(savedReports.size())))
                .andReturn();

        //then
    }

    @Test
    void findAll_happyPath() throws Exception {
        //given
        List<QuestionReportEntity> reports = generateListReports();
        questionRepository.saveAll(reports.stream()
                .map(QuestionReportEntity::getQuestion)
                .collect(Collectors.toList()));

        List<QuestionReportEntity> savedReports = questionReportRepository.saveAll(reports);

        //when
        MvcResult result = mockMvc.perform(get("/reports?page=0&size=10 "))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", hasSize(savedReports.size())))
                .andReturn();

        //then
    }

    @Test
    void create_happyPath() throws Exception {
        //given
        QuestionEntity question = questionRepository.save(generateQuestion());
        ReportCreateDto reportToSave = generateReportCreateDto();
        reportToSave.setQuestionId(question.getId());


        //when
        MvcResult result = mockMvc.perform(post("/reports")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(reportToSave)))
                .andExpect(status().isCreated())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        ReportFullDto savedReport = objectMapper.readValue(bytes, new TypeReference<>() {
        });

        //then
        assertNotNull(savedReport.getId());
        assertEquals(savedReport.getAuthorId(), reportToSave.getAuthorId());
        assertEquals(savedReport.getText(), reportToSave.getText());
        assertEquals(savedReport.getReportStatus(), ReportStatus.OPEN);
        assertEquals(reportToSave.getQuestionId(), savedReport.getQuestionId());
    }

    @Test
    void update_happyPath() throws Exception {
        //given
        QuestionReportEntity reportToSave = generateReportEntity();
        questionRepository.save(reportToSave.getQuestion());

        QuestionReportEntity savedReport = questionReportRepository.save(reportToSave);

        ReportUpdateDto reportToUpdate = ReportUpdateDto.builder()
                .id(savedReport.getId())
                .text("OLOL")
                .reportStatus(ReportStatus.CLOSED)
                .build();


        //when
        MvcResult result = mockMvc.perform(put("/reports")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(reportToUpdate)))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        ReportFullDto updatedReport = objectMapper.readValue(bytes, new TypeReference<>() {
        });

        //then
        assertNotNull(updatedReport);
        assertEquals(updatedReport.getId(), savedReport.getId());
        assertEquals(updatedReport.getAuthorId(), savedReport.getAuthorId());
        assertNotEquals(updatedReport.getText(), savedReport.getText());
        assertNotEquals(updatedReport.getReportStatus(), savedReport.getReportStatus());
    }

    @Test
    void deleteById_happyPath() throws Exception {
        //given
        QuestionReportEntity reportToSave = generateReportEntity();
        questionRepository.save(reportToSave.getQuestion());

        QuestionReportEntity savedReport = questionReportRepository.save(reportToSave);

        //when
        mockMvc.perform(delete("/reports/{id}", savedReport.getId()))
                .andExpect(status().isOk());

        //then
        mockMvc.perform(get("/reports/{id}", savedReport.getId()))
                .andExpect(status().isNotFound());
    }

    @Test
    void findById_whenNotFound() throws Exception {
        //given
        Long notExistingId = 100000L;

        //when
        mockMvc.perform(get("/reports/{id}", notExistingId))
                .andExpect(status().isNotFound());

        //then
    }

    @Test
    void deleteById_whenNotFound() throws Exception {
        //given
        Long notExistingId = 100000L;

        //when
        mockMvc.perform(delete("/reports/{id}", notExistingId))
                .andExpect(status().isNotFound());

        //then
    }

    @Test
    void create_whenQuestionNotFound() throws Exception {
        //given
        ReportCreateDto reportToSave = generateReportCreateDto();
        reportToSave.setQuestionId(100000L);

        //when
        mockMvc.perform(post("/reports")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(reportToSave)))
                .andExpect(status().isNotFound());

        //then
    }

    @Test
    void update_whenNotFound() throws Exception {
        //given
        ReportUpdateDto reportToUpdate = ReportUpdateDto.builder()
                .id(100000L)
                .build();

        //when
        mockMvc.perform(post("/reports")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(reportToUpdate)))
                .andExpect(status().isBadRequest());

        //then
    }

    @Test
    void create_whenInvalidText() throws Exception {
        //given
        ReportCreateDto reportToSave = generateReportCreateDto();
        reportToSave.setText("");

        //when
        mockMvc.perform(post("/reports")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(reportToSave)))
                .andExpect(status().isBadRequest());

        //then
    }

    @Test
    void create_whenInvalidAuthorId() throws Exception {
        //given
        ReportCreateDto reportToSave = generateReportCreateDto();
        reportToSave.setAuthorId(null);

        //when
        mockMvc.perform(post("/reports")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(reportToSave)))
                .andExpect(status().isBadRequest());
        //then
    }

    @Test
    void create_whenInvalidQuestionId() throws Exception {
        //given
        ReportCreateDto reportToSave = generateReportCreateDto();
        reportToSave.setQuestionId(null);

        //when
        mockMvc.perform(post("/reports")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(reportToSave)))
                .andExpect(status().isBadRequest());
        //then
    }

}
