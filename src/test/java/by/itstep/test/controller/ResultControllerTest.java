package by.itstep.test.controller;

import by.itstep.test.dto.result.ResultCreateDto;
import by.itstep.test.dto.result.ResultFullDto;
import by.itstep.test.dto.result.ResultPreviewDto;
import by.itstep.test.entity.OptionEntity;
import by.itstep.test.entity.QuestionEntity;
import by.itstep.test.entity.ResultEntity;
import by.itstep.test.entity.TestEntity;
import by.itstep.test.integration.AbstractIntegrationTest;
import by.itstep.test.repository.QuestionRepository;
import by.itstep.test.repository.ResultRepository;
import by.itstep.test.repository.TestRepository;
import by.itstep.test.service.ResultService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.List;

import static by.itstep.test.util.DtoGeneratorUtils.generateResultCreateDto;
import static by.itstep.test.util.EntityGeneratorUtils.*;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ResultControllerTest extends AbstractIntegrationTest {

    @Test
    void testFindById_happyPath() throws Exception {
        // given
        ResultEntity savedResult = resultRepository.save(generateResult());

        // when
        MvcResult result = mockMvc.perform(get("/results/{id}", savedResult.getId()))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        ResultFullDto foundResult = objectMapper.readValue(bytes, ResultFullDto.class);

        // then
        assertNotNull(foundResult);
        assertNotNull(foundResult.getId());
        assertEquals(foundResult.getId(), savedResult.getId());
        assertEquals(foundResult.getUserId(), savedResult.getUserId());
        assertEquals(foundResult.getScore(), savedResult.getScore());
    }

    @Test
    void testFindByUserIdAndTestId_happyPath() throws Exception {
        //given
        ResultFullDto resultFullDto = resultService.create(getResultCreateDto());
        Long userId = resultFullDto.getUserId();
        Long testId = resultFullDto.getTestPreview().getId();

        //when
        MvcResult result = mockMvc.perform(get("/results/find")
                .param("userID", String.valueOf(userId))
                .param("testID", String.valueOf(testId)))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        List<ResultPreviewDto> foundResults = objectMapper.readValue(bytes, new TypeReference<>() {});

        //then
        assertNotNull(foundResults);
        assertEquals(foundResults.get(0).getTestPreview().getId(), testId);
        assertEquals(foundResults.get(0).getUserId(), userId);
    }

    @Test
    void testFindByUserIdAndTestId_whenTestNotFound() throws Exception {
        //given
        Long notExistingTestId = 1000000L;
        Long notExistingUserId = 1000000L;

        //when
        mockMvc.perform(get(    "/results/find")
        .param("userID", String.valueOf(notExistingUserId))
        .param("testID", String.valueOf(notExistingTestId)))
                .andExpect(status().isNotFound())
                .andReturn();


        //then
    }

    @Test
    void testFindById_whenNotFound() throws Exception {
        //given
        Long notExistingId = 10000000L;

        //when
        mockMvc.perform(get("/results/{id}", notExistingId))
                .andExpect(status().isNotFound());

        //then
    }

    @Test
    void testCreate_happyPath() throws Exception {
        //given
        ResultCreateDto resultToSave = getResultCreateDto();

        //when
        MvcResult result = mockMvc.perform(post("/results")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(resultToSave)))
                .andExpect(status().isCreated())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        ResultFullDto savedResult = objectMapper.readValue(bytes, ResultFullDto.class);

        //then
        assertNotNull(savedResult);
        assertNotNull(savedResult.getId());
        assertEquals(savedResult.getUserId(), resultToSave.getUserId());
        assertEquals(savedResult.getAnswers().size(), resultToSave.getAnswers().size());
    }

    @Test
    void testFindAll_happyPath() throws Exception {
        //given
        List<ResultEntity> savedResult = resultRepository.saveAll(generateListResults());

        //when
        mockMvc.perform(get("/results?page=0&size=10"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", hasSize(savedResult.size())))
                .andReturn();

        //then
    }

    @Test
    void testFindAll_whenPageNullSizeNull() throws Exception {
        //given
        List<ResultEntity> savedResult = resultRepository.saveAll(generateListResults());

        //when
        mockMvc.perform(get("/results"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", hasSize(savedResult.size())))
                .andReturn();

        //then
    }

    @Test
    void testDelete_happyPath() throws Exception {
        //given
        ResultEntity savedResult = resultRepository.save(generateResult());

        //when
        mockMvc.perform(delete("/results/{id}", savedResult.getId()))
                .andExpect(status().isOk());

        //then
    }

    @Test
    void testDeleteById_whenNotFound() throws Exception {
        //given
        Long notExistingId = 10000000L;

        //when
        mockMvc.perform(delete("/results/{id}", notExistingId))
                .andExpect(status().isNotFound());

        //then
    }

    private ResultCreateDto getResultCreateDto() {
        TestEntity test = testRepository.save(generateTest());

        List<QuestionEntity> questions = generateListQuestion();
        for (QuestionEntity question : questions) {
            List<OptionEntity> options = generateListOption();

            question.setOptions(options);
            for (OptionEntity option : options) {
                option.setQuestion(question);
            }
        }
        List<QuestionEntity> savedQuestion = questionRepository.saveAll(questions);

        return generateResultCreateDto(savedQuestion, test);
    }

    @Test
    void testCreate_whenTestIdNull() throws Exception {
        //given
        ResultCreateDto resultCreateDto = generateResultCreateDto();
        resultCreateDto.setTestId(null);

        //when
        mockMvc.perform(post("/results")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(resultCreateDto)))
                .andExpect(status().isBadRequest());

        //then
    }

    @Test
    void testCreate_whenUserIdNull() throws Exception {
        //given
        ResultCreateDto resultCreateDto = generateResultCreateDto();
        resultCreateDto.setUserId(null);

        //when
        mockMvc.perform(post("/results")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(resultCreateDto)))
                .andExpect(status().isBadRequest());

        //then
    }
}