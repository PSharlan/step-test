package by.itstep.test.controller;

import by.itstep.test.dto.test.TestCreateDto;
import by.itstep.test.dto.test.TestFullDto;
import by.itstep.test.dto.test.TestPreviewDto;
import by.itstep.test.dto.test.TestUpdateDto;
import by.itstep.test.entity.QuestionEntity;
import by.itstep.test.entity.TestEntity;
import by.itstep.test.integration.AbstractIntegrationTest;
import by.itstep.test.repository.QuestionRepository;
import by.itstep.test.repository.TestRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.List;

import static by.itstep.test.util.DtoGeneratorUtils.*;
import static by.itstep.test.util.EntityGeneratorUtils.*;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class TestControllerTest extends AbstractIntegrationTest {

    @Test
    void testFindById_happyPath() throws Exception {
        //given
        TestEntity saved = testRepository.save(generateTest());

        //when
        MvcResult result = mockMvc.perform(get("/tests/{id}", saved.getId()))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        TestFullDto foundTest = objectMapper.readValue(bytes, TestFullDto.class);

        //then
        assertNotNull(foundTest);
        assertNotNull(foundTest.getId());
        assertEquals(foundTest.getId(), saved.getId());
        assertEquals(foundTest.getName(), saved.getName());
        assertEquals(foundTest.getLevel(), saved.getLevel());
        assertEquals(foundTest.getAuthorId(), saved.getAuthorId());
        assertEquals(foundTest.getLanguage(), saved.getLanguage());
        assertEquals(foundTest.getDirection(), saved.getDirection());
    }

    @Test
    void testFindAll_happyPath() throws Exception {
        //given
        List<TestEntity> savedTests = testRepository.saveAll(generateListTest());

        //when
            mockMvc.perform(get("/tests?page=0&size=10"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", hasSize(savedTests.size())))
                .andReturn();

        //then
    }

    @Test
    void testFindAll_whenPageNullSizeNull() throws Exception {
        //given
        List<TestEntity> savedTests = testRepository.saveAll(generateListTest());

        //when
        mockMvc.perform(get("/tests"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", hasSize(savedTests.size())))
                .andReturn();

        //then
    }

    @Test
    void testCreate_happyPath() throws Exception {
        //given
        TestCreateDto testToSave = generateTestCreateDto();

        //when
        MvcResult result = mockMvc.perform(post("/tests")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(testToSave)))
                .andExpect(status().isCreated())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        TestFullDto savedTest = objectMapper.readValue(bytes, TestFullDto.class);

        //then
        assertNotNull(savedTest);
        assertNotNull(savedTest.getId());
        assertEquals(savedTest.getName(), testToSave.getName());
        assertEquals(savedTest.getLevel(), testToSave.getLevel());
        assertEquals(savedTest.getLanguage(), testToSave.getLanguage());
        assertEquals(savedTest.getAuthorId(), testToSave.getAuthorId());
        assertEquals(savedTest.getDirection(), testToSave.getDirection());
    }

    @Test
    void testUpdate_happyPath() throws Exception {
        //given
        TestEntity savedTest = testRepository.save(generateTest());

        TestUpdateDto updateTest = generateTestUpdateDto();
        updateTest.setId(savedTest.getId());

        //when
        MvcResult result = mockMvc.perform(put("/tests")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateTest)))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        TestFullDto updatedTest = objectMapper.readValue(bytes, TestFullDto.class);

        //then
        assertNotNull(updatedTest);
        assertEquals(updatedTest.getId(), updateTest.getId());
        assertEquals(updatedTest.getLanguage(), savedTest.getLanguage());
        assertEquals(updatedTest.getAuthorId(), updateTest.getAuthorId());
        assertNotEquals(updatedTest.getDirection(), savedTest.getDirection());
        assertNotEquals(updatedTest.getName(), savedTest.getName());
        assertNotEquals(updatedTest.getRating(), savedTest.getRating());
    }

    @Test
    void testDelete_happyPath() throws Exception {
        //given
        TestEntity savedTest = testRepository.save(generateTest());

        //when
        mockMvc.perform(delete("/tests/{id}", savedTest.getId()))
                .andExpect(status().isOk());

        //then
        mockMvc.perform(get("/tests/{id}", savedTest.getId()))
                .andExpect(status().isNotFound());
    }

    @Test
    void testDeleteById_whenNotFound() throws Exception {
        //given
        Long notExistingId = 10000000L;

        //when
        mockMvc.perform(delete("/tests/{id}", notExistingId))
                .andExpect(status().isNotFound());

        //then
    }

    @Test
    void testFindById_whenNotFound() throws Exception {
        //given
        Long notExistingId = 10000000L;

        //when
        mockMvc.perform(get("/tests/{id}", notExistingId))
                .andExpect(status().isNotFound());

        //then
    }

    @Test
    void testCreate_whenInvalidNumberQuestion() throws Exception {
        //given
        questionRepository.saveAll(generateListQuestion());
        TestCreateDto testCreateDto = generateTestCreateDto();
        testCreateDto.setQuestionIds(List.of(1L, 2L));

        //when
        mockMvc.perform(post("/tests")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(testCreateDto)))
                .andExpect(status().isBadRequest());

        //then
    }

    @Test
    void testUpdate_whenNotFound() throws Exception {
        //given
        TestUpdateDto testToUpdate = generateTestUpdateDto();
        testToUpdate.setQuestionIds(List.of(1L, 2L, 3L));
        testToUpdate.setId(1000000L);

        //when
        mockMvc.perform(put("/tests")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(testToUpdate)))
                .andExpect(status().isNotFound());

        //then
    }

    @Test
    void testUpdate_whenInvalidNumberQuestion() throws Exception {
        //given
        questionRepository.saveAll(generateListQuestion());
        TestEntity savedTest = testRepository.save(generateTest());
        TestUpdateDto updateDto = generateTestUpdateDto();
        updateDto.setId(savedTest.getId());
        updateDto.setQuestionIds(List.of(1L, 2L));

        //when
        mockMvc.perform(post("/tests")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updateDto)))
                .andExpect(status().isBadRequest());

        //then
    }

    @Test
    void testCreate_whenAuthorIdNull() throws Exception {
        //given
        TestCreateDto testCreateDto = generateTestCreateDto();
        testCreateDto.setAuthorId(null);

        //when
        mockMvc.perform(post("/tests")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(testCreateDto)))
                .andExpect(status().isBadRequest());

        //then
    }

    @Test
    void testCreate_whenLanguageNull() throws Exception {
        //given
        TestCreateDto testCreateDto = generateTestCreateDto();
        testCreateDto.setLanguage(null);

        //when
        mockMvc.perform(post("/tests")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(testCreateDto)))
                .andExpect(status().isBadRequest());

        //then
    }

    @Test
    void testCreate_whenDirectionNull() throws Exception {
        //given
        TestCreateDto testCreateDto = generateTestCreateDto();
        testCreateDto.setDirection(null);

        //when
        mockMvc.perform(post("/tests")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(testCreateDto)))
                .andExpect(status().isBadRequest());

        //then
    }

    @Test
    void testCreate_whenLevelNull() throws Exception {
        //given
        TestCreateDto testCreateDto = generateTestCreateDto();
        testCreateDto.setLevel(null);

        //when
        mockMvc.perform(post("/tests")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(testCreateDto)))
                .andExpect(status().isBadRequest());

        //then
    }

    @Test
    void testCreate_whenNameEmpty() throws Exception {
        //given
        TestCreateDto testCreateDto = generateTestCreateDto();
        testCreateDto.setName("");

        //when
        mockMvc.perform(post("/tests")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(testCreateDto)))
                .andExpect(status().isBadRequest());

        //then
    }

    @Test
    void testUpdate_whenRatingEmpty() throws Exception {
        //given
        TestUpdateDto testUpdateDto = generateTestUpdateDto();
        testUpdateDto.setRating(null);

        //when
        mockMvc.perform(put("/tests")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(testUpdateDto)))
                .andExpect(status().isBadRequest());

        //then
    }

    @Test
    void testUpdate_whenRatingMoreMax() throws Exception {
        //given
        TestUpdateDto testUpdateDto = generateTestUpdateDto();
        testUpdateDto.setRating(11);

        //when
        mockMvc.perform(put("/tests")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(testUpdateDto)))
                .andExpect(status().isBadRequest());

        //then
    }

    @Test
    void testUpdate_whenRatingLessMin() throws Exception {
        //given
        TestUpdateDto testUpdateDto = generateTestUpdateDto();
        testUpdateDto.setRating(0);

        //when
        mockMvc.perform(put("/tests")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(testUpdateDto)))
                .andExpect(status().isBadRequest());

        //then
    }

    @Test
    void testUpdate_whenAuthorIdNull() throws Exception {
        //given
        TestUpdateDto testUpdateDto = generateTestUpdateDto();
        testUpdateDto.setAuthorId(null);

        //when
        mockMvc.perform(post("/tests")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(testUpdateDto)))
                .andExpect(status().isBadRequest());

        //then
    }

    @Test
    void testUpdate_whenLevelNull() throws Exception {
        //given
        TestUpdateDto testUpdateDto = generateTestUpdateDto();
        testUpdateDto.setLevel(null);

        //when
        mockMvc.perform(post("/tests")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(testUpdateDto)))
                .andExpect(status().isBadRequest());

        //then
    }

    @Test
    void testUpdate_whenDirectionNull() throws Exception {
        //given
        TestUpdateDto testUpdateDto = generateTestUpdateDto();
        testUpdateDto.setDirection(null);

        //when
        mockMvc.perform(post("/tests")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(testUpdateDto)))
                .andExpect(status().isBadRequest());

        //then
    }

    @Test
    void testUpdate_whenNameEmpty() throws Exception {
        //given
        TestUpdateDto testUpdateDto = generateTestUpdateDto();
        testUpdateDto.setName(null);

        //when
        mockMvc.perform(post("/tests")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(testUpdateDto)))
                .andExpect(status().isBadRequest());

        //then
    }
}