package by.itstep.test.controller;

import by.itstep.test.entity.enums.Direction;
import by.itstep.test.entity.enums.Language;
import by.itstep.test.entity.enums.Level;
import by.itstep.test.integration.AbstractIntegrationTest;
import com.fasterxml.jackson.core.type.TypeReference;
import org.junit.jupiter.api.Test;
import org.springframework.test.web.servlet.MvcResult;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class DictionaryControllerTest extends AbstractIntegrationTest {

    @Test
    void giveLanguage_happyPath() throws Exception {
        //given
        Language[] actualLanguages = Language.values();

        //when
        MvcResult result = mockMvc.perform(get("/dictionary/languages"))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        Language[] expectedLanguages = objectMapper.readValue(bytes, new TypeReference<>() { });

        //then
        assertEquals(expectedLanguages.length, actualLanguages.length);
        for (int i = 0; i < expectedLanguages.length; i++) {
            assertEquals(expectedLanguages[i], actualLanguages[i]);
        }
    }

    @Test
    void giveLevel_happyPath() throws Exception {
        //given
        Level[] actualLevels = Level.values();

        //when
        MvcResult result = mockMvc.perform(get("/dictionary/levels"))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        Level[] expectedLevels = objectMapper.readValue(bytes, new TypeReference<>(){});

        //then
        assertEquals(expectedLevels.length, actualLevels.length);
        for (int i = 0; i < expectedLevels.length; i++) {
            assertEquals(expectedLevels[i], actualLevels[i]);
        }
    }

    @Test
    void giveDirection_happyPath() throws Exception {
        //given
        Direction[] actualDirections = Direction.values();

        //when
        MvcResult result = mockMvc.perform(get("/dictionary/directions"))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        Direction[] expectedDirections = objectMapper.readValue(bytes, new TypeReference<>(){});

        //then
        assertEquals(expectedDirections.length, actualDirections.length);
        for (int i = 0; i < expectedDirections.length; i++) {
            assertEquals(expectedDirections[i], actualDirections[i]);
        }
    }
}
