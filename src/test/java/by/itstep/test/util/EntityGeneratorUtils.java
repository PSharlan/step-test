package by.itstep.test.util;

import by.itstep.test.entity.*;
import by.itstep.test.entity.enums.Direction;
import by.itstep.test.entity.enums.Language;
import by.itstep.test.entity.enums.Level;
import by.itstep.test.entity.enums.Status;
import by.itstep.test.entity.enums.*;
import com.github.javafaker.Faker;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class EntityGeneratorUtils {

    private static Faker faker = new Faker();
    private static List<ReportStatus> reports = Arrays.asList(ReportStatus.values());

    public static AnswerEntity generateAnswer() {
        return AnswerEntity.builder()
                .isCorrect(true)
                .build();
    }

    public static List<AnswerEntity> generateListAnswers() {
        AnswerEntity answer1 = AnswerEntity.builder()
                .isCorrect(true)
                .build();

        AnswerEntity answer2 = AnswerEntity.builder()
                .isCorrect(true)
                .build();

        AnswerEntity answer3 = AnswerEntity.builder()
                .isCorrect(false)
                .build();

        return List.of(answer1, answer2, answer3);
    }

    public static QuestionEntity generateQuestion() {
        return QuestionEntity.builder()
                .authorId(1L)
                .status(Status.NEW)
                .language(Language.JAVA)
                .type(QuestionType.SELECT_ONE)
                .level(Level.EASY)
                .text("Which operator is comparing objects?")
                .codeBlock("String str = new String();")
                .options(generateListOption())
                .build();
    }

    public static List<QuestionEntity> generateListQuestion() {
        QuestionEntity question1 = QuestionEntity.builder()
                .authorId(1L)
                .language(Language.JAVA)
                .level(Level.EASY)
                .type(QuestionType.SELECT_ONE)
                .status(Status.APPROVED)
                .text("Which operator allows you to compare objects?")
                .codeBlock("String str = new String();")
                .build();

        QuestionEntity question2 = QuestionEntity.builder()
                .authorId(1L)
                .language(Language.JAVA)
                .level(Level.EASY)
                .type(QuestionType.SELECT_ONE)
                .status(Status.APPROVED)
                .text("Is the return statement required in a void method?")
                .codeBlock("Integer a = Integer.parseInt(str);")
                .build();

        QuestionEntity question3 = QuestionEntity.builder()
                .authorId(1L)
                .language(Language.JAVA)
                .level(Level.MEDIUM)
                .type(QuestionType.SELECT_ONE)
                .status(Status.APPROVED)
                .text("Is it possible to create a try block without catch ()?")
                .codeBlock("BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));")
                .build();

        return List.of(question1, question2, question3);
    }

    public static ResultEntity generateResult() {
        return ResultEntity.builder()
                .userId(1L)
                .score(10)
                .timeSpent(72000000L)
                .build();
    }

    public static List<ResultEntity> generateListResults() {
        ResultEntity result1 = ResultEntity.builder()
                .userId(1L)
                .score(10)
                .timeSpent(72000000L)
                .build();

        ResultEntity result2 = ResultEntity.builder()
                .userId(2L)
                .score(9)
                .timeSpent(72000000L)
                .build();

        ResultEntity result3 = ResultEntity.builder()
                .userId(3L)
                .score(8)
                .timeSpent(72000000L)
                .build();

        return List.of(result1, result2, result3);
    }

    public static TestEntity generateTest() {
        return TestEntity.builder()
                .authorId(1L)
                .language(Language.JAVA)
                .direction(Direction.BACKEND)
                .level(Level.EASY)
                .name("Java Core")
                .rating(10)
                .status(Status.PUBLISHED)
                .build();
    }

    public static TestEntity generateTestFromMockito() {
        return TestEntity.builder()
                .id(1L)
                .authorId(1L)
                .language(Language.JAVA)
                .direction(Direction.BACKEND)
                .level(Level.EASY)
                .name("Java Core")
                .rating(10)
                .questions(Collections.emptyList())
                .status(Status.PUBLISHED)
                .build();
    }

    public static List<TestEntity> generateListTest() {
        TestEntity test1 = TestEntity.builder()
                .authorId(1L)
                .language(Language.JAVA)
                .direction(Direction.BACKEND)
                .level(Level.EASY)
                .name("Java Core")
                .rating(10)
                .status(Status.PUBLISHED)
                .build();

        TestEntity test2 = TestEntity.builder()
                .authorId(2L)
                .language(Language.JAVASCRIPT)
                .direction(Direction.FRONTEND)
                .level(Level.MEDIUM)
                .name("JavaScript Core")
                .rating(9)
                .status(Status.PUBLISHED)
                .build();

        TestEntity test3 = TestEntity.builder()
                .authorId(3L)
                .language(Language.PYTHON)
                .direction(Direction.BACKEND)
                .level(Level.HIGH)
                .name("Phyton Core")
                .rating(8)
                .status(Status.PUBLISHED)
                .build();

        return List.of(test1, test2, test3);
    }

    public static List<TestEntity> generateListTestFromMockito() {
        TestEntity test1 = TestEntity.builder()
                .id(1L)
                .authorId(1L)
                .language(Language.JAVA)
                .direction(Direction.BACKEND)
                .level(Level.EASY)
                .name("Java Core")
                .rating(10)
                .status(Status.PUBLISHED)
                .build();

        TestEntity test2 = TestEntity.builder()
                .id(2L)
                .authorId(2L)
                .language(Language.JAVASCRIPT)
                .direction(Direction.FRONTEND)
                .level(Level.MEDIUM)
                .name("JavaScript Core")
                .rating(9)
                .status(Status.PUBLISHED)
                .build();

        TestEntity test3 = TestEntity.builder()
                .id(3L)
                .authorId(3L)
                .language(Language.PYTHON)
                .direction(Direction.BACKEND)
                .level(Level.HIGH)
                .name("Phyton Core")
                .rating(8)
                .status(Status.PUBLISHED)
                .build();

        return List.of(test1, test2, test3);
    }

    public static OptionEntity generationOption() {

        return OptionEntity.builder()
                .isCorrect(true)
                .text("YES")
                .build();
    }

    public static List<OptionEntity> generateListOption() {
        OptionEntity option1 = OptionEntity.builder()
                .isCorrect(false)
                .text("new Test().run();")
                .build();

        OptionEntity option2 = OptionEntity.builder()
                .isCorrect(false)
                .text("Thread thread = new Thread(new Test()).start();")
                .build();

        OptionEntity option3 = OptionEntity.builder()
                .isCorrect(true)
                .text("new Test(new Thread()).run();")
                .build();

        OptionEntity option4 = OptionEntity.builder()
                .isCorrect(false)
                .text("Other")
                .build();

        return List.of(option1, option2, option3, option4);
    }

    public static ResultEntity generateResultFullEntity() {
        ResultEntity entity = generateResult();
        TestEntity test = generateTest();
        test.setQuestions(generateListQuestion());
        test.setId(1L);

        entity.setId(1L);
        entity.setTest(test);
        entity.setAnswers(generateListAnswers());

        return entity;
    }

    public static QuestionEntity generateQuestionFullEntity() {
        QuestionEntity entity = generateQuestion();
        entity.setId(1L);
        entity.setOptions(generateListOption());

        return entity;
    }

    public static TestEntity generateTestFullEntity() {
        TestEntity testEntity = generateTest();
        List<QuestionEntity> questions = generateListQuestion();
        for (QuestionEntity question :
                questions) {
            Long count = 1L;
            question.setId(count);
            ++count;
        }
        testEntity.setQuestions(questions);

        return testEntity;
    }

    public static AnswerEntity generateAnswerFullEntity() {
        AnswerEntity answer = generateAnswer();
        OptionEntity option = generationOption();
        QuestionEntity question = generateQuestion();
        ResultEntity result = generateResult();

        option.setId(1L);
        question.setId(1L);
        result.setId(1L);

        answer.setResult(result);
        answer.setQuestion(question);
        answer.setChosenOption(option);

        return answer;
    }

    public static QuestionReportEntity generateReportEntity() {
        return QuestionReportEntity.builder()
                .authorId(faker.number().numberBetween(0L, 100L))
                .reportStatus(reports.get(faker.number().numberBetween(0, 1)))
                .text("Two identical answers (options)")
                .question(EntityGeneratorUtils.generateQuestion())
                .build();
    }

    public static List<QuestionReportEntity> generateListReports() {
        return List.of(generateReportEntity(), generateReportEntity(), generateReportEntity());
    }
}
