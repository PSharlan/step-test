package by.itstep.test.util;

import by.itstep.test.dto.answer.AnswerCreateDto;
import by.itstep.test.dto.option.OptionCreateDto;
import by.itstep.test.dto.question.QuestionCreateDto;
import by.itstep.test.dto.question.QuestionUpdateDto;
import by.itstep.test.dto.report.ReportCreateDto;
import by.itstep.test.dto.result.ResultCreateDto;
import by.itstep.test.dto.test.TestCreateDto;
import by.itstep.test.dto.test.TestUpdateDto;
import by.itstep.test.entity.QuestionEntity;
import by.itstep.test.entity.TestEntity;
import by.itstep.test.entity.enums.*;
import com.github.javafaker.Faker;

import java.util.ArrayList;
import java.util.List;

import static by.itstep.test.mapper.QuestionMapper.QUESTION_MAPPER;
import static by.itstep.test.util.EntityGeneratorUtils.generateQuestion;

public class DtoGeneratorUtils {

    private static Faker faker = new Faker();

    public static ResultCreateDto generateResultCreateDto(List<QuestionEntity> savedQuestions, TestEntity test) {
        List<AnswerCreateDto> answers = new ArrayList<>();
        for (QuestionEntity savedQuestion : savedQuestions) {
            AnswerCreateDto answer = new AnswerCreateDto();
            answer.setQuestionId(savedQuestion.getId());

            answer.setOptionId(savedQuestion.getOptions().get(0).getId());

            answers.add(answer);
        }

        ResultCreateDto resultCreateDto = new ResultCreateDto();
        resultCreateDto.setAnswers(answers);
        resultCreateDto.setTestId(test.getId());
        resultCreateDto.setUserId(1L);
        resultCreateDto.setTimeSpent(72000000L);

        return resultCreateDto;
    }

    public static OptionCreateDto generateOptionCreateDto() {
        OptionCreateDto optionCreateDto = new OptionCreateDto();

        optionCreateDto.setText("Yes");
        optionCreateDto.setIsCorrect(false);

        return optionCreateDto;
    }

    public static List<OptionCreateDto> generateListOptionCreateDto() {
        OptionCreateDto optionCreateDto1 = new OptionCreateDto();
        OptionCreateDto optionCreateDto2 = new OptionCreateDto();

        optionCreateDto1.setText("Yes");
        optionCreateDto1.setIsCorrect(false);

        optionCreateDto2.setText("No");
        optionCreateDto2.setIsCorrect(true);

        return List.of(optionCreateDto1, optionCreateDto2);
    }

    public static List<OptionCreateDto> generateNewListOptionCreateDto() {
        OptionCreateDto optionCreateDto1 = new OptionCreateDto();
        OptionCreateDto optionCreateDto2 = new OptionCreateDto();
        OptionCreateDto optionCreateDto3 = new OptionCreateDto();

        optionCreateDto1.setText("==");
        optionCreateDto1.setIsCorrect(false);

        optionCreateDto3.setText("equals()");
        optionCreateDto3.setIsCorrect(true);

        optionCreateDto2.setText("=");
        optionCreateDto2.setIsCorrect(false);

        return List.of(optionCreateDto1, optionCreateDto2, optionCreateDto3);
    }

    public static AnswerCreateDto generateAnswerCreateDto() {
        AnswerCreateDto answerCreateDto = new AnswerCreateDto();

        answerCreateDto.setOptionId(1L);
        answerCreateDto.setQuestionId(1L);

        return answerCreateDto;
    }

    public static List<AnswerCreateDto> generateListAnswerCreateDto() {
        AnswerCreateDto answerCreateDto1 = new AnswerCreateDto();
        AnswerCreateDto answerCreateDto2 = new AnswerCreateDto();

        answerCreateDto1.setOptionId(1L);
        answerCreateDto1.setQuestionId(1L);

        answerCreateDto2.setOptionId(2L);
        answerCreateDto2.setQuestionId(2L);

        return List.of(answerCreateDto1, answerCreateDto1);
    }

    public static TestCreateDto generateTestCreateDto() {
        TestCreateDto createDto = new TestCreateDto();
        createDto.setLanguage(Language.JAVASCRIPT);
        createDto.setDirection(Direction.BACKEND);
        createDto.setLevel(Level.EASY);
        createDto.setAuthorId(1L);
        createDto.setName("Java Core");

        return createDto;
    }

    public static TestUpdateDto generateTestUpdateDto() {
        TestUpdateDto updateDto = new TestUpdateDto();
        updateDto.setName("JavaScript Syntax");
        updateDto.setDirection(Direction.FRONTEND);
        updateDto.setLevel(Level.EASY);
        updateDto.setRating(8);
        updateDto.setAuthorId(1L);
        updateDto.setStatus(Status.HIDDEN);

        return updateDto;
    }

    public static QuestionCreateDto generateQuestionCreateDto() {
        QuestionCreateDto createDto = new QuestionCreateDto();

        createDto.setAuthorId(2L);
        createDto.setText("What language are you learning?");
        createDto.setLanguage(Language.GO);
        createDto.setLevel(Level.EASY);
        createDto.setType(QuestionType.SELECT_ONE);
        createDto.setOptions(generateListOptionCreateDto());

        return createDto;
    }

    public static QuestionUpdateDto generateQuestionUpdateDto() {
        QuestionUpdateDto updateDto = new QuestionUpdateDto();

        updateDto.setAuthorId(1L);
        updateDto.setStatus(Status.APPROVED);
        updateDto.setLanguage(Language.JAVA);
        updateDto.setLevel(Level.MEDIUM);
        updateDto.setType(QuestionType.SELECT_ONE);
        updateDto.setOptions(generateNewListOptionCreateDto());
        updateDto.setText("What programming language are you studying?");

        return updateDto;
    }

    public static ResultCreateDto generateResultCreateDto() {
        ResultCreateDto result = new ResultCreateDto();

        result.setAnswers(generateListAnswerCreateDto());
        result.setTestId(1L);
        result.setUserId(1L);
        result.setTimeSpent(72000000L);

        return result;
    }

    public static ReportCreateDto generateReportCreateDto() {
        return ReportCreateDto.builder()
                .authorId(faker.number().numberBetween(0L, 100L))
                .text("Two identical answers (options)")
                .questionId(QUESTION_MAPPER.mapToFullDto(generateQuestion()).getId())
                .build();
    }
}
