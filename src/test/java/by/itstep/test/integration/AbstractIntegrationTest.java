package by.itstep.test.integration;

import by.itstep.test.StepTestApplication;
import by.itstep.test.dto.achievement.PlayerProgressFullDto;
import by.itstep.test.entity.enums.Action;
import by.itstep.test.integration.initalizer.MySqlInitializer;
import by.itstep.test.repository.*;
import by.itstep.test.service.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.testcontainers.junit.jupiter.Testcontainers;

@ContextConfiguration(initializers = MySqlInitializer.class,
        classes = StepTestApplication.class)
@SpringBootTest
@Testcontainers
@AutoConfigureMockMvc
public abstract class AbstractIntegrationTest {

    @Autowired
    protected QuestionReportService reportService;

    @Autowired
    protected OptionRepository optionRepository;

    @Autowired
    protected ResultRepository resultRepository;

    @Autowired
    protected QuestionRepository questionRepository;

    @Autowired
    protected QuestionReportRepository questionReportRepository;

    @Autowired
    protected TestRepository testRepository;

    @Autowired
    protected AnswerRepository answerRepository;

    @Autowired
    protected QuestionReportService questionReportService;

    @Autowired
    protected QuestionService questionService;

    @Autowired
    protected ResultService resultService;

    @Autowired
    protected TestService testService;

    @Autowired
    protected MockMvc mockMvc;

    @Autowired
    protected ObjectMapper objectMapper;

    @MockBean
    protected StepAchievementsClient achievementsClient;

    protected static PlayerProgressFullDto playerProgressFullDto;

    @BeforeAll
    public static void initPlayerProgressFullDto() {
        playerProgressFullDto = new PlayerProgressFullDto();
        playerProgressFullDto.setId(1);
        playerProgressFullDto.setPlayerId(1);
        playerProgressFullDto.setAction(Action.TEST_PASSING);
        playerProgressFullDto.setCounterAction(1);
    }

    @BeforeEach
    public void prepareDatabase() {
        resultRepository.deleteAll();
        testRepository.deleteAll();
        questionRepository.deleteAll();
        optionRepository.deleteAll();
        answerRepository.deleteAll();
        questionReportRepository.deleteAll();
    }
}
