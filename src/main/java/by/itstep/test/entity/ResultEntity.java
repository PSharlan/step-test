package by.itstep.test.entity;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "results")
public class ResultEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "test_id")
    private TestEntity test;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "score")
    private int score;

    @Column(name = "time_spent")
    private Long timeSpent;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "result",fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<AnswerEntity> answers = new ArrayList();
}
