package by.itstep.test.entity;

import by.itstep.test.entity.enums.Language;
import by.itstep.test.entity.enums.Level;
import by.itstep.test.entity.enums.QuestionType;
import by.itstep.test.entity.enums.Status;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "questions")
public class QuestionEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "text", nullable = false)
    private String text;

    @Column(name = "author_id")
    private Long authorId;

    @Column(name = "code_block")
    private String codeBlock;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "question")
    private List<AnswerEntity> answers = new ArrayList<>();

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToMany(mappedBy = "questions")
    private List<TestEntity> tests = new ArrayList<>();

    @Column(name = "status")
    @Enumerated(value = EnumType.STRING)
    private Status status;

    @Column(name = "language")
    @Enumerated(value = EnumType.STRING)
    private Language language;

    @Column(name = "level")
    @Enumerated(value = EnumType.STRING)
    private Level level;

    @Column(name = "type")
    @Enumerated(value = EnumType.STRING)
    private QuestionType type;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "question", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private List<QuestionReportEntity> reports = new ArrayList<>();

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "question", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<OptionEntity> options = new ArrayList<>();
}
