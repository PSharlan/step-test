package by.itstep.test.entity;

import by.itstep.test.entity.enums.Direction;
import by.itstep.test.entity.enums.Language;
import by.itstep.test.entity.enums.Level;
import by.itstep.test.entity.enums.Status;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "tests")
public class TestEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "language", nullable = false)
    @Enumerated(value = EnumType.STRING)
    private Language language;

    @Column(name = "direction")
    @Enumerated(value = EnumType.STRING)
    private Direction direction;

    @Column(name = "level", nullable = false)
    @Enumerated(value = EnumType.STRING)
    private Level level;

    @Column(name = "status", nullable = false)
    @Enumerated(value = EnumType.STRING)
    private Status status;

    @Column(name = "rating")
    private Integer rating;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToMany
    @JoinTable(
            name = "test_question",
            joinColumns = { @JoinColumn(name = "test_id") },
            inverseJoinColumns = { @JoinColumn(name = "question_id")}
    )
    private List<QuestionEntity> questions = new ArrayList<>();

    @Column(name = "author_id", nullable = false)
    private Long authorId;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "test")
    private List<ResultEntity> results = new ArrayList<>();
}
