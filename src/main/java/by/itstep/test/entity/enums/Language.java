package by.itstep.test.entity.enums;

public enum  Language {

    JAVASCRIPT,
    PYTHON,
    JAVA,
    RUBY,
    PHP,
    GO,
    C
}
