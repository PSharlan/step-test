package by.itstep.test.entity.enums;

public enum Status {

    APPROVED,
    NEW,
    REJECTED,
    PUBLISHED,
    HIDDEN
}
