package by.itstep.test.entity.enums;

public enum Direction {

    BACKEND,
    FRONTEND,
    FULLSTECK,
    QA,
    BUSINES_ANALYST
}
