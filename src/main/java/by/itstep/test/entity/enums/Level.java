package by.itstep.test.entity.enums;

public enum Level {

    EASY,
    MEDIUM,
    HIGH
}
