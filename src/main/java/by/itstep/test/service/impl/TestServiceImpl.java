package by.itstep.test.service.impl;

import by.itstep.test.dto.test.TestCreateDto;
import by.itstep.test.dto.test.TestFullDto;
import by.itstep.test.dto.test.TestPreviewDto;
import by.itstep.test.dto.test.TestUpdateDto;
import by.itstep.test.entity.QuestionEntity;
import by.itstep.test.entity.TestEntity;
import by.itstep.test.entity.enums.Action;
import by.itstep.test.entity.enums.Status;
import by.itstep.test.exception.NumberQuestionException;
import by.itstep.test.exception.TestNotFoundException;
import by.itstep.test.repository.QuestionRepository;
import by.itstep.test.repository.TestRepository;
import by.itstep.test.service.StepAchievementsClient;
import by.itstep.test.service.TestService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static by.itstep.test.mapper.TestMapper.*;

@Slf4j
@Service
@RequiredArgsConstructor
public class TestServiceImpl implements TestService {

    private final TestRepository testRepository;
    private final QuestionRepository questionRepository;
    private final StepAchievementsClient achievementsClient;

    @Override
    @Transactional(readOnly = true)
    public TestFullDto findById(Long id) {
        TestFullDto foundTest = testRepository.findById(id)
                .map(TEST_MAPPER::mapToFullDto)
                .orElseThrow(() -> new TestNotFoundException(id));
        foundTest.setNumberOfPassed(testRepository.getNumberPassed(id).orElse(0));
        foundTest.setAverageScore(testRepository.getAverageScore(id).orElse(0D));

        log.info("TestServiceImpl -> found Test: {} by id: {}", foundTest, id);
        return foundTest;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<TestPreviewDto> findAll(Integer page, Integer size) {
        PageRequest pageRequest = PageRequest.of(page == null ? 0 : page,
                                size == null ? Integer.MAX_VALUE : size);
        Page<TestPreviewDto> foundTests = testRepository.findAll(pageRequest)
                .map(TEST_MAPPER::mapToPreviewDto);

        foundTests.forEach(testPreviewDto -> {
            testPreviewDto.setNumberOfPassed(testRepository.getNumberPassed(testPreviewDto.getId()).orElse(0));
            testPreviewDto.setAverageScore(testRepository.getAverageScore(testPreviewDto.getId()).orElse(0D));
        });

        log.info("TestServiceImpl -> found {} tests", foundTests.getNumberOfElements());
        return foundTests;
    }

    @Override
    @Transactional
    public TestFullDto create(TestCreateDto testCreateDto) {
        TestEntity testToSave = TEST_MAPPER.mapToEntity(testCreateDto);

        List<QuestionEntity> questions = questionRepository.findAllById(testCreateDto.getQuestionIds());

        validateNumberQuestion(questions, testCreateDto.getQuestionIds());

        testToSave.setQuestions(questions);
        testToSave.setStatus(Status.NEW);

        TestFullDto testFullDto = saveTest(questions, testToSave);

        log.info("TestServiceImpl -> test {} successfully saved", testFullDto);
        return testFullDto;
    }

    @Override
    @Transactional
    public TestFullDto update(TestUpdateDto testUpdateDto) {
        TestEntity availableTest = testRepository.findById(testUpdateDto.getId())
                .orElseThrow(() -> new TestNotFoundException(testUpdateDto.getId()));

        List<QuestionEntity> questions = questionRepository.findAllById(testUpdateDto.getQuestionIds());

        validateNumberQuestion(questions, testUpdateDto.getQuestionIds());

        availableTest.getQuestions().clear();

        availableTest.setDirection(testUpdateDto.getDirection());
        availableTest.setAuthorId(testUpdateDto.getAuthorId());
        availableTest.setLevel(testUpdateDto.getLevel());
        availableTest.setName(testUpdateDto.getName());
        availableTest.setAuthorId(testUpdateDto.getAuthorId());
        availableTest.setRating(testUpdateDto.getRating());
        availableTest.setQuestions(questions);
        availableTest.setStatus(testUpdateDto.getStatus());

        TestFullDto fullDto = saveTest(questions, availableTest);

        log.info("TestServiceImpl -> test {} was successfully updated", fullDto.getId());
        return fullDto;
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        testRepository.findById(id).orElseThrow(() -> new TestNotFoundException(id));

        testRepository.deleteById(id);
    }

    private void validateNumberQuestion(List<QuestionEntity> foundListQuestion, List<Long> originalListQuestion) {
        if (foundListQuestion.size() != originalListQuestion.size()) {
            throw new NumberQuestionException(String.format("Wrong number of questions. Found : %s, sought : %s",
                    foundListQuestion.size(), originalListQuestion.size()));
        }
    }

    private TestFullDto saveTest(List<QuestionEntity> questions, TestEntity availableTest) {
        questions.forEach(question -> question.setTests(List.of(availableTest)));

        TestEntity test = testRepository.save(availableTest);
        achievementsClient.recordAchievement(Action.TEST_CREATING, test.getAuthorId());


        TestFullDto testFullDto = TEST_MAPPER.mapToFullDto(test);
        testFullDto.setNumberOfPassed(testRepository.getNumberPassed(test.getId()).orElse(0));
        testFullDto.setAverageScore(testRepository.getAverageScore(test.getId()).orElse(0D));

        return testFullDto;
    }
}