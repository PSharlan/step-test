package by.itstep.test.service.impl;

import by.itstep.test.dto.result.ResultCreateDto;
import by.itstep.test.dto.result.ResultFullDto;
import by.itstep.test.dto.result.ResultPreviewDto;
import by.itstep.test.entity.*;
import by.itstep.test.entity.enums.Action;
import by.itstep.test.exception.OptionNotFoundException;
import by.itstep.test.exception.QuestionNotFoundException;
import by.itstep.test.exception.ResultNotFoundException;
import by.itstep.test.exception.TestNotFoundException;
import by.itstep.test.repository.OptionRepository;
import by.itstep.test.repository.QuestionRepository;
import by.itstep.test.repository.ResultRepository;
import by.itstep.test.repository.TestRepository;
import by.itstep.test.service.ResultService;
import by.itstep.test.service.StepAchievementsClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static by.itstep.test.mapper.ResultMapper.RESULT_MAPPER;
import static java.util.stream.Collectors.toList;

@Slf4j
@Service
@RequiredArgsConstructor
public class ResultServiceImpl implements ResultService {

    private final ResultRepository resultRepository;
    private final TestRepository testRepository;
    private final OptionRepository optionRepository;
    private final QuestionRepository questionRepository;
    private final StepAchievementsClient achievementsClient;

    @Override
    @Transactional(readOnly = true)
    public ResultFullDto findById(Long id) {
        ResultEntity foundResult = resultRepository.findById(id)
                .orElseThrow(() -> new ResultNotFoundException(id));

        log.info("ResultServiceImpl -> found result: {} start by id: {}", foundResult, id);
        return RESULT_MAPPER.mapToFullDto(foundResult);
    }

    @Override
    public List<ResultPreviewDto> findByUserIdAndTestId(Long userId, Long testId) {
        checkUserIdAndTestId(userId, testId);

        List<ResultEntity> foundResults =
                resultRepository.getResultByUserAndTestId(userId, testId);

        foundResults.forEach(result -> {
            List<QuestionEntity> questions = questionRepository.findByTestId(result.getTest().getId());
            result.getTest().setQuestions(questions);
        });

        log.info("ResultServiceImpl -> found results: {}", foundResults.size());
        return RESULT_MAPPER.mapToPreviewDtoList(foundResults);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ResultPreviewDto> findAll(Integer page, Integer size) {
        PageRequest pageRequest = PageRequest.of(page == null ? 0 : page,
                size == null ? Integer.MAX_VALUE : size);
        Page<ResultEntity> foundResults = resultRepository.findAll(pageRequest);

        log.info("ResultServiceImp; -> found {} results", foundResults.getNumberOfElements());
        return foundResults.map(RESULT_MAPPER::mapToPreviewDto);
    }

    @Override
    @Transactional
    public ResultFullDto create(ResultCreateDto resultCreateDto) {
        ResultEntity resultToSave = RESULT_MAPPER.mapToEntity(resultCreateDto);

        TestEntity test = testRepository.findById(resultCreateDto.getTestId())
                .orElseThrow(() -> new TestNotFoundException(resultCreateDto.getTestId()));
        resultToSave.setTest(test);

        List<AnswerEntity> answers = resultCreateDto.getAnswers()
                .stream()
                .map(answer -> {
                    AnswerEntity answerEntity = new AnswerEntity();

                    QuestionEntity question = questionRepository.findById(answer.getQuestionId())
                            .orElseThrow(() -> new QuestionNotFoundException(answer.getQuestionId()));

                    OptionEntity option = optionRepository.findById(answer.getOptionId())
                            .orElseThrow(() -> new OptionNotFoundException(answer.getOptionId()));

                    answerEntity.setIsCorrect(option.getIsCorrect());
                    answerEntity.setQuestion(question);

                    return answerEntity;
                })
                .collect(toList());

        resultToSave.setAnswers(answers);
        answers.forEach(answer -> answer.setResult(resultToSave));

        ResultEntity savedResult = resultRepository.save(resultToSave);
        achievementsClient.recordAchievement(Action.TEST_PASSING, savedResult.getUserId());

        log.info("ResultServiceImpl -> result {} successfully saved", savedResult);
        return RESULT_MAPPER.mapToFullDto(savedResult);
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        resultRepository.findById(id).orElseThrow(() -> new ResultNotFoundException(id));

        resultRepository.deleteById(id);
    }

    private boolean firstPassedTest(ResultEntity result) {
        return resultRepository.getResultByUserAndTestId(result.getUserId(), result.getTest().getId()).isEmpty();
    }

    private void checkUserIdAndTestId(Long userId, Long testId) {
        testRepository.findById(testId)
                .orElseThrow(() -> new TestNotFoundException(testId));
        //TODO - add check userId with RestTemplate
    }
}