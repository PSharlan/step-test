package by.itstep.test.service.impl;

import by.itstep.test.dto.report.ReportCreateDto;
import by.itstep.test.dto.report.ReportFullDto;
import by.itstep.test.dto.report.ReportPreviewDto;
import by.itstep.test.dto.report.ReportUpdateDto;
import by.itstep.test.entity.QuestionEntity;
import by.itstep.test.entity.QuestionReportEntity;
import by.itstep.test.entity.enums.ReportStatus;
import by.itstep.test.exception.QuestionNotFoundException;
import by.itstep.test.exception.ReportNotFoundException;
import by.itstep.test.repository.QuestionReportRepository;
import by.itstep.test.repository.QuestionRepository;
import by.itstep.test.service.QuestionReportService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static by.itstep.test.mapper.QuestionReportMapper.*;

@Slf4j
@Service
@RequiredArgsConstructor
public class QuestionReportServiceImpl implements QuestionReportService {

    private final QuestionReportRepository questionReportRepository;
    private final QuestionRepository questionRepository;

    @Override
    @Transactional(readOnly = true)
    public ReportFullDto findById(Long id) {
        QuestionReportEntity foundReport = questionReportRepository.findById(id)
                .orElseThrow(() -> new ReportNotFoundException(id));

        log.info("QuestionReportServiceImpl -> found report {}, by id: {}", foundReport, id);
        return QUESTION_REPORT_MAPPER.mapToFullDto(foundReport);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ReportPreviewDto> findAll(Integer page, Integer size) {
        PageRequest pageRequest = PageRequest.of(page == null ? 0 : page,
                                size == null ? Integer.MAX_VALUE : size);
        Page<QuestionReportEntity> foundReports = questionReportRepository.findAll(pageRequest);

        log.info("QuestionReportServiceImpl -> found reports {}", foundReports.getNumberOfElements());
        return foundReports.map(QUESTION_REPORT_MAPPER::mapToPreviewDto);
    }

    @Override
    @Transactional
    public ReportFullDto create(ReportCreateDto createDto) {
        QuestionReportEntity reportToSave = QUESTION_REPORT_MAPPER.mapToEntity(createDto);
        QuestionEntity question = questionRepository.findById(createDto.getQuestionId())
                .orElseThrow(() -> new QuestionNotFoundException(createDto.getQuestionId()));

        reportToSave.setQuestion(question);
        reportToSave.setReportStatus(ReportStatus.OPEN);

        QuestionReportEntity savedReport = questionReportRepository.save(reportToSave);

        log.info("QuestionReportServiceImpl -> Report {} successfully saved", savedReport);
        return QUESTION_REPORT_MAPPER.mapToFullDto(savedReport);
    }

    @Override
    @Transactional
    public ReportFullDto update(ReportUpdateDto updateDto) {
        QuestionReportEntity availableReport = questionReportRepository.findById(updateDto.getId())
                .orElseThrow(() -> new ReportNotFoundException(updateDto.getId()));

        availableReport.setText(updateDto.getText());
        availableReport.setReportStatus(updateDto.getReportStatus());

        QuestionReportEntity updatedReport = questionReportRepository.save(availableReport);


        log.info("QuestionReportServiceImpl -> Report by id {} was successfully updated", updatedReport);
        return QUESTION_REPORT_MAPPER.mapToFullDto(updatedReport);
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        if(!questionReportRepository.existsById(id)) {
            throw new ReportNotFoundException(id);
        }

        log.info("QuestionReportServiceImpl -> Report by id {} was successfully deleted", id);
        questionReportRepository.deleteById(id);
    }
}
