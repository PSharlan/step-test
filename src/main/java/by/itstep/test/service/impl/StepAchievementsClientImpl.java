package by.itstep.test.service.impl;

import by.itstep.test.dto.achievement.PlayerCommittedActionDto;
import by.itstep.test.dto.achievement.PlayerProgressFullDto;
import by.itstep.test.entity.enums.Action;
import by.itstep.test.service.StepAchievementsClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.transaction.Transactional;

@Slf4j
@Service
public class StepAchievementsClientImpl implements StepAchievementsClient {

    @Value("${remote.step-achievement-url}")
    private String stepAchievementUrl;

    @Value("#{new Boolean('${remote.achievement.enable}')}")
    private Boolean enable;

    private static final String REQUEST_URL = "/playerProgress/processAction";

    @Override
    @Transactional
    public PlayerProgressFullDto recordAchievement(Action action, Long userId) {
        if (!enable) {
            log.info("StepAchievementsClientImpl -> don`t record achievement. Message sending is disabled(enable = false)");
            return null;
        }
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<PlayerCommittedActionDto> request = new HttpEntity<>(
                new PlayerCommittedActionDto(action, userId));

        log.info("StepAchievementsClientImpl -> record achievement by user: {}", userId);
        return restTemplate.
                postForEntity(stepAchievementUrl + REQUEST_URL, request, PlayerProgressFullDto.class)
                .getBody();
    }
}
