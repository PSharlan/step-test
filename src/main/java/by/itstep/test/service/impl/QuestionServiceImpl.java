package by.itstep.test.service.impl;

import by.itstep.test.dto.question.QuestionCreateDto;
import by.itstep.test.dto.question.QuestionFullDto;
import by.itstep.test.dto.question.QuestionPreviewDto;
import by.itstep.test.dto.question.QuestionUpdateDto;
import by.itstep.test.entity.OptionEntity;
import by.itstep.test.entity.QuestionEntity;
import by.itstep.test.entity.enums.Status;
import by.itstep.test.exception.CountCorrectOptionException;
import by.itstep.test.exception.QuestionNotFoundException;
import by.itstep.test.repository.OptionRepository;
import by.itstep.test.repository.QuestionRepository;
import by.itstep.test.service.QuestionService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static by.itstep.test.mapper.OptionMapper.OPTION_MAPPER;
import static by.itstep.test.mapper.QuestionMapper.QUESTION_MAPPER;

@Slf4j
@Service
@RequiredArgsConstructor
public class QuestionServiceImpl implements QuestionService {

    private final QuestionRepository questionRepository;
    private final OptionRepository optionRepository;

    @Override
    @Transactional(readOnly = true)
    public QuestionFullDto findById(Long id) {
        QuestionEntity foundQuestion = questionRepository.findById(id)
                .orElseThrow(() -> new QuestionNotFoundException(id));

        log.info("QuestionServiceImpl -> found question: {} by id: {}", foundQuestion, id);
        return QUESTION_MAPPER.mapToFullDto(foundQuestion);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<QuestionPreviewDto> findAll(Integer page, Integer size) {
        PageRequest pageRequest = PageRequest.of(page == null ? 0 : page,
                                size == null ? Integer.MAX_VALUE : size);
        Page<QuestionEntity> foundQuestions = questionRepository.findAll(pageRequest);

        log.info("QuestionServiceImpl -> found {} questions", foundQuestions.getNumberOfElements());
        return foundQuestions.map(QUESTION_MAPPER::mapToPreviewDto);
    }

    @Override
    @Transactional
    public QuestionFullDto create(QuestionCreateDto questionCreateDto) {
        QuestionEntity questionToSave = QUESTION_MAPPER.mapToEntity(questionCreateDto);

        List<OptionEntity> options = questionToSave.getOptions();
        validateCountCorrectOption(options);

        questionToSave.setStatus(Status.NEW);
        questionToSave.setOptions(options);
        options.forEach(option -> option.setQuestion(questionToSave));

        QuestionEntity savedQuestion = questionRepository.save(questionToSave);

        log.info("QuestionServiceImpl -> question {} successfully saved", savedQuestion);
        return QUESTION_MAPPER.mapToFullDto(savedQuestion);
    }

    @Override
    @Transactional
    public QuestionFullDto update(QuestionUpdateDto questionUpdateDto) {
        QuestionEntity availableQuestion = questionRepository.findById(questionUpdateDto.getId()).
                orElseThrow(() -> new QuestionNotFoundException(questionUpdateDto.getId()));

        List<OptionEntity> options = OPTION_MAPPER.mapToEntityList(questionUpdateDto.getOptions());
        validateCountCorrectOption(options);

        optionRepository.deleteAll(availableQuestion.getOptions());
        availableQuestion.getOptions().clear();

        availableQuestion.setText(questionUpdateDto.getText());
        availableQuestion.setAuthorId(questionUpdateDto.getAuthorId());
        availableQuestion.setStatus(questionUpdateDto.getStatus());
        availableQuestion.setCodeBlock(questionUpdateDto.getCodeBlock());
        availableQuestion.setOptions(options);
        availableQuestion.setLanguage(questionUpdateDto.getLanguage());
        availableQuestion.setLevel(questionUpdateDto.getLevel());
        availableQuestion.setType(questionUpdateDto.getType());
        options.forEach(option -> option.setQuestion(availableQuestion));

        QuestionEntity updatedQuestion = questionRepository.save(availableQuestion);

        log.info("QuestionServiceImpl -> questions {} was successfully updated", updatedQuestion.getId());
        return QUESTION_MAPPER.mapToFullDto(updatedQuestion);
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        questionRepository.findById(id).orElseThrow(() -> new QuestionNotFoundException(id));

        questionRepository.deleteById(id);
    }

    private void validateCountCorrectOption(List<OptionEntity> options) {
        int countCorrectOption = 0;
        for (OptionEntity option : options) {
            if (option.getIsCorrect()) {
                countCorrectOption++;
            }
            if (countCorrectOption > 1) {
                throw new CountCorrectOptionException("One Correct Option Needed.");
            }
        }
    }
}