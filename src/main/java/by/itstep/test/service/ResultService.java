package by.itstep.test.service;

import by.itstep.test.dto.result.ResultCreateDto;
import by.itstep.test.dto.result.ResultFullDto;
import by.itstep.test.dto.result.ResultPreviewDto;
import org.springframework.data.domain.Page;

import java.util.List;

public interface ResultService {

    ResultFullDto findById(Long id);

    List<ResultPreviewDto> findByUserIdAndTestId(Long userId, Long testId);

    Page<ResultPreviewDto> findAll(Integer page, Integer size);

    ResultFullDto create(ResultCreateDto createDto);

    void deleteById(Long id);
}