package by.itstep.test.service;

import by.itstep.test.dto.report.ReportCreateDto;
import by.itstep.test.dto.report.ReportFullDto;
import by.itstep.test.dto.report.ReportPreviewDto;
import by.itstep.test.dto.report.ReportUpdateDto;
import org.springframework.data.domain.Page;

public interface QuestionReportService {

    ReportFullDto findById(Long id);

    Page<ReportPreviewDto> findAll(Integer page, Integer size);

    ReportFullDto create(ReportCreateDto createDto);

    ReportFullDto update(ReportUpdateDto updateDto);

    void deleteById(Long id);

}
