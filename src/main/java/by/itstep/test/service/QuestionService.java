package by.itstep.test.service;

import by.itstep.test.dto.question.QuestionCreateDto;
import by.itstep.test.dto.question.QuestionFullDto;
import by.itstep.test.dto.question.QuestionPreviewDto;
import by.itstep.test.dto.question.QuestionUpdateDto;
import org.springframework.data.domain.Page;

public interface QuestionService {

    QuestionFullDto findById(Long id);

    Page<QuestionPreviewDto> findAll(Integer page, Integer size);

    QuestionFullDto create(QuestionCreateDto createDto);

    QuestionFullDto update(QuestionUpdateDto updateDto);

    void deleteById(Long id);
}