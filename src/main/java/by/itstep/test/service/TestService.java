package by.itstep.test.service;

import by.itstep.test.dto.test.TestCreateDto;
import by.itstep.test.dto.test.TestFullDto;
import by.itstep.test.dto.test.TestPreviewDto;
import by.itstep.test.dto.test.TestUpdateDto;
import org.springframework.data.domain.Page;

public interface TestService {

    TestFullDto findById(Long id);

    Page<TestPreviewDto> findAll(Integer page, Integer size);

    TestFullDto create(TestCreateDto createDto);

    TestFullDto update(TestUpdateDto updateDto);

    void deleteById(Long id);
}