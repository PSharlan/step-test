package by.itstep.test.service;

import by.itstep.test.dto.achievement.PlayerProgressFullDto;
import by.itstep.test.entity.enums.Action;

public interface StepAchievementsClient {

    PlayerProgressFullDto recordAchievement(Action action, Long userId);

}
