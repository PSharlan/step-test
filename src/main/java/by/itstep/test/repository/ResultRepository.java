package by.itstep.test.repository;

import by.itstep.test.entity.ResultEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ResultRepository extends JpaRepository<ResultEntity, Long> {

    @Query(value = "SELECT * FROM results r WHERE r.test_id = :testId AND r.user_id = :userId", nativeQuery = true)
    List<ResultEntity> getResultByUserAndTestId(@Param("userId") Long userId,
                                                  @Param("testId") Long testId);
}
