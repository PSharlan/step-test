package by.itstep.test.repository;

import by.itstep.test.entity.AnswerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

public interface AnswerRepository extends JpaRepository<AnswerEntity, Long> {
}
