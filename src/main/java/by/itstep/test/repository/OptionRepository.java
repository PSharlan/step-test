package by.itstep.test.repository;

import by.itstep.test.entity.OptionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

public interface OptionRepository extends JpaRepository<OptionEntity, Long> {
}
