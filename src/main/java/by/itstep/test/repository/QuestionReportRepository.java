package by.itstep.test.repository;

import by.itstep.test.entity.QuestionReportEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QuestionReportRepository extends JpaRepository<QuestionReportEntity, Long> {
}
