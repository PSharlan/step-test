package by.itstep.test.repository;

import by.itstep.test.entity.TestEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface TestRepository extends JpaRepository<TestEntity, Long> {

    @Query(value = "SELECT ROUND(AVG(r.score), 2) FROM tests t JOIN results r ON t.id = r.test_id WHERE t.id = :id",
            nativeQuery = true)
    Optional<Double> getAverageScore(@Param("id") Long id);

    @Query(value = "SELECT COUNT(r.id) FROM tests t JOIN results r ON t.id = r.test_id WHERE t.id = :id",
            nativeQuery = true)
    Optional<Integer> getNumberPassed(@Param("id") Long id);
}
