package by.itstep.test.repository;

import by.itstep.test.entity.QuestionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface QuestionRepository extends JpaRepository<QuestionEntity, Long> {

    @Query(value = "SELECT * FROM questions q " +
            "JOIN test_question t_q ON q.id = t_q.question_id " +
            "JOIN tests t ON t.id = t_q.test_id " +
            "WHERE t.id = :testId", nativeQuery = true)
    List<QuestionEntity> findByTestId(@Param("testId") Long testId);
}
