package by.itstep.test.dto.question;

import by.itstep.test.dto.option.OptionCreateDto;
import by.itstep.test.entity.enums.Language;
import by.itstep.test.entity.enums.Level;
import by.itstep.test.entity.enums.QuestionType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Data
public class QuestionCreateDto {

    @NotBlank(message = "text can't be empty" )
    @ApiModelProperty(example = "Which operator is comparing objects?", notes = "Question text")
    private String text;

    @NotBlank(message = "codeBlock can`t be empty")
    @ApiModelProperty(example = "String str = new String();", notes = "question code block")
    private String codeBlock;

    @NotNull(message = "authorId can't be null" )
    @ApiModelProperty(example = "1", notes = "Id of the author who created the question")
    private Long authorId;

    @NotNull(message = "language can't be null" )
    @ApiModelProperty(example = "JAVA", notes = "Language question")
    private Language language;

    @NotNull(message = "type can't be null" )
    @ApiModelProperty(example = "SELECT_ONE", notes = "Type question")
    private QuestionType type;

    @NotNull(message = "level can't be null" )
    @ApiModelProperty(example = "EASY", notes = "Level question")
    private Level level;

    @ApiModelProperty(notes = "Only one option must be correct")
    private List<OptionCreateDto> options = new ArrayList<>();

}
