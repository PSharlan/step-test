package by.itstep.test.dto.question;

import by.itstep.test.entity.enums.Language;
import by.itstep.test.entity.enums.Level;
import by.itstep.test.entity.enums.QuestionType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class QuestionPreviewDto {

    @ApiModelProperty(example = "1", notes = "Id of the current question")
    private Long id;

    @ApiModelProperty(example = "Which operator is comparing objects?", notes = "Question text")
    private String text;

    @ApiModelProperty(example = "Java", notes = "Question language")
    private Language language;

    @ApiModelProperty(example = "Easy", notes = "Question level")
    private Level level;

    @ApiModelProperty(example = "SINGLE_ONE", notes = "Question type")
    private QuestionType type;

}
