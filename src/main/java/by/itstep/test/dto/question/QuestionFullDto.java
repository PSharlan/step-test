package by.itstep.test.dto.question;

import by.itstep.test.dto.option.OptionFullDto;
import by.itstep.test.entity.enums.Language;
import by.itstep.test.entity.enums.Level;
import by.itstep.test.entity.enums.QuestionType;
import by.itstep.test.entity.enums.Status;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class QuestionFullDto {

    @ApiModelProperty(example = "1", notes = "Id of the current question")
    private Long id;

    @ApiModelProperty(example = "Which operator is comparing objects?", notes = "Question text")
    private String text;

    @ApiModelProperty(example = "String str = new String();", notes = "question code block")
    private String codeBlock;

    @ApiModelProperty(example = "1", notes = "Id of the author who created the question")
    private Long authorId;

    @ApiModelProperty(example = "NEW", notes = "Question status")
    private Status status;

    @ApiModelProperty(example = "Java", notes = "Question language")
    private Language language;

    @ApiModelProperty(example = "Easy", notes = "Question level")
    private Level level;

    @ApiModelProperty(example = "SINGLE_ONE", notes = "Question type")
    private QuestionType type;

    @ApiModelProperty(notes = "Only one option must be correct")
    private List<OptionFullDto> options = new ArrayList<>();

}
