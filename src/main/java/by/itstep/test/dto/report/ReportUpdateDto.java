package by.itstep.test.dto.report;

import by.itstep.test.entity.enums.ReportStatus;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Builder
public class ReportUpdateDto {

    @NotNull
    @ApiModelProperty(example = "1", notes = "Id of the current report")
    private Long id;

    @NotBlank
    @ApiModelProperty(example = "Two identical answers", notes = "Report text")
    private String text;

    @NotNull
    @ApiModelProperty(example = "OPEN", notes = "Report statys ")
    private ReportStatus reportStatus;

}
