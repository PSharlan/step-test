package by.itstep.test.dto.report;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Builder
public class ReportCreateDto {

    @NotBlank
    @ApiModelProperty(example = "Two identical answers", notes = "Report text")
    private String text;

    @NotNull
    @ApiModelProperty(example = "10", notes = "Report author id")
    private Long authorId;

    @NotNull
    @ApiModelProperty(notes = "The id of the question for which the complaint was submitted")
    private Long questionId;

}
