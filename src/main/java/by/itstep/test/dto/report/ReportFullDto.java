package by.itstep.test.dto.report;

import by.itstep.test.entity.enums.ReportStatus;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ReportFullDto {

    @ApiModelProperty(example = "1", notes = "Id of the current report")
    private Long id;

    @ApiModelProperty(example = "10", notes = "Report author id")
    private Long authorId;

    @ApiModelProperty(example = "Two identical answers", notes = "Report text")
    private String text;

    @ApiModelProperty(notes = "id of the report question")
    private Long questionId;

    @ApiModelProperty(example = "OPEN", notes = "Report statys ")
    private ReportStatus reportStatus;

}
