package by.itstep.test.dto.result;

import by.itstep.test.dto.answer.AnswerFullDto;
import by.itstep.test.dto.test.TestPreviewDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ResultFullDto {

    @ApiModelProperty(example = "1", notes = "Id of this option")
    private Long id;

    @ApiModelProperty(notes = "Preview test of the passed test")
    private TestPreviewDto testPreview;

    @ApiModelProperty(example = "2", notes = "User ID of responses")
    private Long userId;

    @ApiModelProperty(example = "10", notes = "Points for the passed test based on correct answers")
    private Integer score;

    @ApiModelProperty(example = "72000000", notes = "Time spent on test")
    private Long timeSpent;

    @ApiModelProperty(notes = "Answers to the passed test of the passing user")
    private List<AnswerFullDto> answers = new ArrayList<>();

}
