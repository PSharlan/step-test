package by.itstep.test.dto.result;

import by.itstep.test.dto.answer.AnswerCreateDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Data
public class ResultCreateDto {

    @NotNull(message = "textId can't be null" )
    @ApiModelProperty(example = "1", notes = "Id of the passed test")
    private Long testId;

    @NotNull(message = "userId can't be null" )
    @ApiModelProperty(example = "2", notes = "User ID of responses")
    private Long userId;

    @NotNull(message = "timeSpent can`t be null")
    @ApiModelProperty(example = "72000000", notes = "Time spent on test")
    private Long timeSpent;

    @ApiModelProperty(notes = "Answers to the passed test of the passing user")
    private List<AnswerCreateDto> answers = new ArrayList<>();

}
