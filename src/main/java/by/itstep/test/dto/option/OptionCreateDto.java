package by.itstep.test.dto.option;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

@Data
public class OptionCreateDto {

    @NotBlank(message = "text can't be empty" )
    @ApiModelProperty(example = "equals()", notes = "Option text that is a possible answer to the question")
    private String text;

    @NotEmpty(message = "isCorrect can't be empty" )
    @ApiModelProperty(example = "true", notes = "Correct or incorrect given answer")
    private Boolean isCorrect;

}
