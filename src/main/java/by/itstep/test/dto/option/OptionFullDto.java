package by.itstep.test.dto.option;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


@Data
public class OptionFullDto {

    @ApiModelProperty(example = "1", notes = "Id of this option")
    private Long id;

    @ApiModelProperty(example = "equals()", notes = "Option text that is a possible answer to the question")
    private String text;

    @ApiModelProperty(example = "true", notes = "Correct or incorrect given answer")
    private Boolean isCorrect;

}
