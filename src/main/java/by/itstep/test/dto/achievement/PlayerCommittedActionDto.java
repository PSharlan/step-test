package by.itstep.test.dto.achievement;

import by.itstep.test.entity.enums.Action;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PlayerCommittedActionDto {

    private Action action;
    private Long userId;
}
