package by.itstep.test.dto.achievement;

import by.itstep.test.entity.enums.Action;
import lombok.Data;

@Data
public class PlayerProgressFullDto {

    private Integer id;
    private Action action;
    private Integer counterAction;
    private Integer playerId;

}
