package by.itstep.test.dto.answer;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class AnswerCreateDto {

    @NotNull(message = "questionId can't be null")
    @ApiModelProperty(example = "1", notes = "Existing id must be specified")
    private Long questionId;

    @NotNull(message = "optionId can't be null")
    @ApiModelProperty(example = "2", notes = "Existing id must be specified")
    private Long optionId;

}
