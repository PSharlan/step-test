package by.itstep.test.dto.answer;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class AnswerFullDto {

    @ApiModelProperty(example = "1", notes = "Id of this answer")
    private Long id;

    @ApiModelProperty(example = "1", notes = "Existing id must be specified")
    private Long questionId;

    @ApiModelProperty(example = "2", notes = "Existing id must be specified")
    private Long optionId;

    @ApiModelProperty(example = "3", notes = "Existing id must be specified")
    private Long resultId;

    @ApiModelProperty(example = "true", notes = "Correct or incorrect answer is recorded based on the selected option")
    private Boolean isCorrect;

}
