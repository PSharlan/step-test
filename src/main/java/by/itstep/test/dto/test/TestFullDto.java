package by.itstep.test.dto.test;

import by.itstep.test.dto.question.QuestionFullDto;
import by.itstep.test.entity.enums.Direction;
import by.itstep.test.entity.enums.Language;
import by.itstep.test.entity.enums.Level;
import by.itstep.test.entity.enums.Status;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class TestFullDto {

    @ApiModelProperty(example = "1", notes = "Id of this test")
    private Long id;

    @ApiModelProperty(example = "5", notes = "Rating of this test")
    private Integer rating;

    @ApiModelProperty(example = "1", notes = "Test author id")
    private Long authorId;

    @ApiModelProperty(example = "Bob", notes = "The name of the user who created the test")
    private String name;

    @ApiModelProperty(example = "JAVA", notes = "Language test")
    private Language language;

    @ApiModelProperty(example = "BACKEND", notes = "Test description")
    private Direction direction;

    @ApiModelProperty(example = "NEW", notes = "Test status")
    private Status status;

    @ApiModelProperty(example = "EASY", notes = "Test difficulty level")
    private Level level;

    @ApiModelProperty(notes = "List of question for test")
    private List<QuestionFullDto> questions = new ArrayList<>();

    @ApiModelProperty(notes = "Number of test passes")
    private Integer numberOfPassed;

    @ApiModelProperty(notes = "Average score")
    private Double averageScore;

}