package by.itstep.test.dto.test;

import by.itstep.test.entity.enums.Direction;
import by.itstep.test.entity.enums.Language;
import by.itstep.test.entity.enums.Level;
import by.itstep.test.entity.enums.Status;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Data
public class TestUpdateDto {

    @ApiModelProperty(example = "1", notes = "Id of this test")
    private Long id;

    @Min(1)
    @Max(10)
    @NotNull(message = "rating can`t be null")
    @ApiModelProperty(example = "5", notes = "Rating of this test")
    private Integer rating;

    @NotNull(message = "authorId can`t be null")
    @ApiModelProperty(example = "1", notes = "Test author id")
    private Long authorId;

    @NotEmpty(message = "name can`t be empty")
    @ApiModelProperty(example = "Bob", notes = "The name of the user who created the test")
    private String name;

    @NotNull(message = "direction can`t be null")
    @ApiModelProperty(example = "BACKEND", notes = "Test description")
    private Direction direction;

    @NotNull(message = "status can't be null" )
    @ApiModelProperty(example = "NEW", notes = "Test status")
    private Status status;

    @NotNull(message = "level can`t be null")
    @ApiModelProperty(example = "EASY", notes = "Test difficulty level")
    private Level level;

    @ApiModelProperty(notes = "List of question for test")
    private List<Long> questionIds = new ArrayList<>();

}
