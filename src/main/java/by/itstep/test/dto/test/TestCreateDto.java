package by.itstep.test.dto.test;

import by.itstep.test.entity.enums.Direction;
import by.itstep.test.entity.enums.Language;
import by.itstep.test.entity.enums.Level;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Data
public class TestCreateDto {

    @NotNull(message = "authorId can`t be null")
    @ApiModelProperty(example = "1", notes = "Test author id")
    private Long authorId;

    @NotBlank(message = "name can`t be empty")
    @ApiModelProperty(example = "Bob", notes = "The name of the user who created the test")
    private String name;

    @NotNull(message = "language can`t be null")
    @ApiModelProperty(example = "JAVA", notes = "Language test")
    private Language language;

    @NotNull(message = "direction can`t be null")
    @ApiModelProperty(example = "BACKEND", notes = "Test description")
    private Direction direction;

    @NotNull(message = "level can`t be null")
    @ApiModelProperty(example = "EASY", notes = "Test difficulty level")
    private Level level;

    @ApiModelProperty(notes = "List of question for test")
    private List<Long> questionIds = new ArrayList<>();

}
