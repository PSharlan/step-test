package by.itstep.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StepTestApplication {
    public static void main(String[] args) {
        SpringApplication.run(StepTestApplication.class, args);
    }

}
