package by.itstep.test.mapper;

import by.itstep.test.dto.test.TestCreateDto;
import by.itstep.test.dto.test.TestFullDto;
import by.itstep.test.dto.test.TestPreviewDto;
import by.itstep.test.entity.TestEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(uses = QuestionMapper.class)
public interface TestMapper {

    TestMapper TEST_MAPPER = Mappers.getMapper(TestMapper.class);

    TestFullDto mapToFullDto(TestEntity testEntity);

    @Mapping(target = "numberOfQuestions", expression = "java(testEntity.getQuestions().size() )")
    TestPreviewDto mapToPreviewDto(TestEntity testEntity);

    @Mapping(target = "questions", ignore = true)
    TestEntity mapToEntity(TestCreateDto createDto);

    List<TestPreviewDto> mapToPreviewDtoList(List<TestEntity> testEntities);
}
