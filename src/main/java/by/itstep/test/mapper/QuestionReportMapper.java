package by.itstep.test.mapper;

import by.itstep.test.dto.report.ReportCreateDto;
import by.itstep.test.dto.report.ReportFullDto;
import by.itstep.test.dto.report.ReportPreviewDto;
import by.itstep.test.entity.QuestionReportEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(uses = QuestionMapper.class)
public interface QuestionReportMapper {

    QuestionReportMapper QUESTION_REPORT_MAPPER = Mappers.getMapper(QuestionReportMapper.class);

    @Mapping(target = "questionId", source = "question.id")
    ReportFullDto mapToFullDto(QuestionReportEntity questionReport);

    @Mapping(target = "questionId", source = "question.id")
    ReportPreviewDto mapToPreviewDto(QuestionReportEntity questionReport);

    @Mapping(target = "question", ignore = true)
    QuestionReportEntity mapToEntity(ReportCreateDto createDto);

    List<ReportPreviewDto> mapToListPreviewDto(List<QuestionReportEntity> questionReports);

}
