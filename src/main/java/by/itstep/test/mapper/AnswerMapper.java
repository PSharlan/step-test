package by.itstep.test.mapper;

import by.itstep.test.dto.answer.AnswerCreateDto;
import by.itstep.test.dto.answer.AnswerFullDto;
import by.itstep.test.entity.AnswerEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface AnswerMapper {

    AnswerMapper ANSWER_MAPPER = Mappers.getMapper(AnswerMapper.class);

    @Mapping(target = "questionId", source = "question.id")
    @Mapping(target = "optionId", source = "chosenOption.id")
    @Mapping(target = "resultId", source = "result.id")
    AnswerFullDto mapToFullDto(AnswerEntity answerEntity);

    @Mapping(target = "question.id", source = "questionId")
    @Mapping(target = "chosenOption.id", source = "optionId")
    AnswerEntity mapToEntity(AnswerCreateDto createDto);

    List<AnswerEntity> mapToEntityList(List<AnswerCreateDto> answerCreateDtos);
}
