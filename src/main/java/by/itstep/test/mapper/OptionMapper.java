package by.itstep.test.mapper;

import by.itstep.test.dto.option.OptionCreateDto;
import by.itstep.test.dto.option.OptionFullDto;
import by.itstep.test.entity.OptionEntity;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface OptionMapper {

    OptionMapper OPTION_MAPPER = Mappers.getMapper(OptionMapper.class);

    OptionFullDto mapToFullDto(OptionEntity optionEntity);

    OptionEntity mapToEntity(OptionCreateDto createDto);

    List<OptionEntity> mapToEntityList(List<OptionCreateDto> optionCreateDtos);
}
