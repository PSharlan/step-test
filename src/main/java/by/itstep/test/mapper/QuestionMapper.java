package by.itstep.test.mapper;

import by.itstep.test.dto.option.OptionCreateDto;
import by.itstep.test.dto.option.OptionFullDto;
import by.itstep.test.dto.question.QuestionCreateDto;
import by.itstep.test.dto.question.QuestionFullDto;
import by.itstep.test.dto.question.QuestionPreviewDto;
import by.itstep.test.dto.question.QuestionUpdateDto;
import by.itstep.test.entity.OptionEntity;
import by.itstep.test.entity.QuestionEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static by.itstep.test.mapper.OptionMapper.OPTION_MAPPER;

@Mapper(uses = OptionMapper.class)
public interface QuestionMapper {

    QuestionMapper QUESTION_MAPPER = Mappers.getMapper(QuestionMapper.class);

    QuestionPreviewDto mapToPreviewDto(QuestionEntity questionEntity);

    QuestionFullDto mapToFullDto(QuestionEntity questionEntity);

    QuestionEntity mapToEntity(QuestionCreateDto createDto);

    List<QuestionPreviewDto> mapToPreviewDtoList(List<QuestionEntity> questionEntities);

    List<QuestionFullDto> mapToFullDtoList(List<QuestionEntity> questionEntities);
}
