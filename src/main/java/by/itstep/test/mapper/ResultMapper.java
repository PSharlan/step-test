package by.itstep.test.mapper;

import by.itstep.test.dto.result.ResultCreateDto;
import by.itstep.test.dto.result.ResultFullDto;
import by.itstep.test.dto.result.ResultPreviewDto;
import by.itstep.test.entity.ResultEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(uses = {TestMapper.class, AnswerMapper.class})
public interface ResultMapper {

    ResultMapper RESULT_MAPPER = Mappers.getMapper(ResultMapper.class);

    @Mapping(target = "testPreview", source = "test")
    ResultFullDto mapToFullDto(ResultEntity resultEntity);

    @Mapping(target = "testPreview", source = "test")
    ResultPreviewDto mapToPreviewDto(ResultEntity resultEntity);

    @Mapping(target = "test", ignore = true)
    ResultEntity mapToEntity(ResultCreateDto resultCreateDto);

    List<ResultPreviewDto> mapToPreviewDtoList(List<ResultEntity> resultEntities);
}
