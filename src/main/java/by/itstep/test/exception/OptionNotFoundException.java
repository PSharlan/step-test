package by.itstep.test.exception;

public class OptionNotFoundException extends EntityIsNotFoundException {

    public OptionNotFoundException(Long id) {
        super("Option", id);
    }
}
