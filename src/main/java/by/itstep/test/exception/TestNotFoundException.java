package by.itstep.test.exception;

public class TestNotFoundException extends EntityIsNotFoundException {

    public TestNotFoundException(Long id) {
        super("Test", id);
    }

}
