package by.itstep.test.exception;

public class ResultNotFoundException extends EntityIsNotFoundException {

    public ResultNotFoundException(Long id) {
        super("Result", id);
    }

}
