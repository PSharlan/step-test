package by.itstep.test.exception;

public class NumberQuestionException extends RuntimeException {

    public NumberQuestionException(String message) {
        super(message);
    }

}
