package by.itstep.test.exception;

public class CountCorrectOptionException extends RuntimeException {

    public CountCorrectOptionException(String message) {
        super(message);
    }

}
