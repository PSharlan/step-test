package by.itstep.test.exception;

public class AnswerNotFoundException extends EntityIsNotFoundException {

    public AnswerNotFoundException(Long id) {
        super("Answer", id);
    }
}
