package by.itstep.test.exception;

public class ReportNotFoundException extends EntityIsNotFoundException{

    public ReportNotFoundException(Long id) {
        super("QuestionReport", id);
    }

}
