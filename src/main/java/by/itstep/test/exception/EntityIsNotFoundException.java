package by.itstep.test.exception;

public class EntityIsNotFoundException extends RuntimeException {

    public EntityIsNotFoundException(String entity, Long id) {
        super(entity + " was not found by id: " + id);
    }

}
