package by.itstep.test.exception;

public class QuestionNotFoundException extends EntityIsNotFoundException {

    public QuestionNotFoundException(Long id) {
        super("Question", id);
    }
}
