package by.itstep.test.controller;

import by.itstep.test.entity.enums.Direction;
import by.itstep.test.entity.enums.Language;
import by.itstep.test.entity.enums.Level;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/dictionary")
@Api(description = "Controller responsible for getting enum")
public class DictionaryController {

    @GetMapping("/languages")
    @ApiOperation(value = "Gives a list of languages")
    public Language[] getLanguages() {
        return Language.values();
    }

    @GetMapping("/levels")
    @ApiOperation(value = "Gives a list of level")
    public Level[] getLevels() {
        return Level.values();
    }

    @GetMapping("/directions")
    @ApiOperation(value = "Gives a list of direction")
    public Direction[] getDescriptions() {
        return Direction.values();
    }
}
