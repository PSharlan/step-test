package by.itstep.test.controller;

import by.itstep.test.dto.test.TestCreateDto;
import by.itstep.test.dto.test.TestFullDto;
import by.itstep.test.dto.test.TestPreviewDto;
import by.itstep.test.dto.test.TestUpdateDto;
import by.itstep.test.service.TestService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/tests")
@Api(description = "Controller dedicated to manage tests")
public class TestController {

    private final TestService testService;

    public TestController(TestService testService) {
        this.testService = testService;
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Find one test by id", notes = "Existing id must be specifiend")
    public TestFullDto getById(@PathVariable Long id) {
        return testService.findById(id);
    }

    @GetMapping
    @ApiOperation(value = "Find all tests")
    public Page<TestPreviewDto> findAll(@RequestParam(required = false) Integer page,
                                        @RequestParam(required = false) Integer size) {
        return testService.findAll(page, size);
    }

    @PostMapping
    @ApiOperation(value = "Create tests")
    public ResponseEntity<TestFullDto> create(@Valid @RequestBody TestCreateDto createDto) {
        return new ResponseEntity<>(testService.create(createDto), HttpStatus.CREATED);
    }

    @PutMapping
    @ApiOperation(value = "Update tests")
    public TestFullDto update(@Valid @RequestBody TestUpdateDto updateDto) {
        return testService.update(updateDto);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete test by id", notes = "Existing id must be specifiend")
    public void deleteById(@PathVariable Long id) {
        testService.deleteById(id);
    }
}