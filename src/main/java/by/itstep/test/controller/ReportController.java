package by.itstep.test.controller;

import by.itstep.test.dto.report.ReportCreateDto;
import by.itstep.test.dto.report.ReportFullDto;
import by.itstep.test.dto.report.ReportPreviewDto;
import by.itstep.test.dto.report.ReportUpdateDto;
import by.itstep.test.service.QuestionReportService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/reports")
@Api(description = "Controller dedicated to manage reports to questions")
public class ReportController {

    private final QuestionReportService reportService;

    @GetMapping("/{id}")
    @ApiOperation(value = "Find one report by id", notes = "Existing id must be specified")
    public ReportFullDto getById(@PathVariable Long id) {
        return reportService.findById(id);
    }

    @GetMapping
    @ApiOperation(value = "Find all reports")
    public Page<ReportPreviewDto> findAll(@RequestParam(required = false) Integer page,
                                             @RequestParam(required = false) Integer size) {
        return reportService.findAll(page, size);
    }

    @PostMapping
    @ApiOperation(value = "Create report")
    public ResponseEntity<ReportFullDto> create(@Valid @RequestBody ReportCreateDto createDto) {
        return new ResponseEntity<ReportFullDto>(reportService.create(createDto), HttpStatus.CREATED);
    }

    @PutMapping
    @ApiOperation(value = "Update test")
    public ReportFullDto update(@Valid @RequestBody ReportUpdateDto updateDto) {
        return reportService.update(updateDto);
    }


    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete by id")
    public void deleteById(@PathVariable Long id) {
        reportService.deleteById(id);
    }

}
