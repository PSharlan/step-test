package by.itstep.test.controller;

import by.itstep.test.dto.result.ResultCreateDto;
import by.itstep.test.dto.result.ResultFullDto;
import by.itstep.test.dto.result.ResultPreviewDto;
import by.itstep.test.service.ResultService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/results")
@Api(description = "Controller dedicated to manage results")
public class ResultController {

    private final ResultService resultService;

    public ResultController(ResultService resultService) {
        this.resultService = resultService;
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Find one result by id", notes = "Existing id must be specified")
    public ResultFullDto getById(@PathVariable Long id) {
        return resultService.findById(id);
    }

    @GetMapping
    @ApiOperation(value = "Find all results")
    public Page<ResultPreviewDto> findAll(@RequestParam(required = false) Integer page,
                                          @RequestParam(required = false) Integer size) {
        return resultService.findAll(page, size);
    }

    @PostMapping
    @ApiOperation(value = "Create result")
    public ResponseEntity<ResultFullDto> create(@Valid @RequestBody ResultCreateDto createDto) {
        return new ResponseEntity<>(resultService.create(createDto), HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete result by id", notes = "Existing id must be specifiend")
    public void delete(@PathVariable Long id) {
        resultService.deleteById(id);
    }

    @GetMapping("/find")
    @ApiOperation(value = "Find results of a specific user on a specific test")
    public List<ResultPreviewDto> findByUserIdAndResultId(@RequestParam(value = "userID", required = false) Long userId,
                                                          @RequestParam(value = "testID", required = false) Long testId) {
        return resultService.findByUserIdAndTestId(userId, testId);
    }
}