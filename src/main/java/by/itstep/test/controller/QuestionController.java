package by.itstep.test.controller;

import by.itstep.test.dto.question.QuestionCreateDto;
import by.itstep.test.dto.question.QuestionFullDto;
import by.itstep.test.dto.question.QuestionPreviewDto;
import by.itstep.test.dto.question.QuestionUpdateDto;
import by.itstep.test.service.QuestionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/questions")
@Api(description = "Controller dedicated to manage questions")
public class QuestionController {

    private final QuestionService questionService;

    public QuestionController(QuestionService questionService) {
        this.questionService = questionService;
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Find one question by id", notes = "Existing id must be specifiend")
    public QuestionFullDto getById(@PathVariable Long id) {
        return questionService.findById(id);
    }

    @GetMapping
    @ApiOperation(value = "Find all questions")
    public Page<QuestionPreviewDto> findAll(@RequestParam(required = false) Integer page,
                                            @RequestParam(required = false) Integer size) {
        return questionService.findAll(page, size);
    }

    @PostMapping
    @ApiOperation(value = "Create question")
    public ResponseEntity<QuestionFullDto> create(@Valid @RequestBody QuestionCreateDto createDto) {
        return new ResponseEntity<>(questionService.create(createDto), HttpStatus.CREATED);
    }

    @PutMapping
    @ApiOperation(value = "Update question")
    public QuestionFullDto update(@Valid @RequestBody QuestionUpdateDto updateDto) {
        return questionService.update(updateDto);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete question by id", notes = "Existing id must be specifiend")
    public void delete(@PathVariable Long id) {
        questionService.deleteById(id);
    }
}