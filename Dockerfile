FROM openjdk:11

ADD target/*.jar /app_step-test.jar

ENTRYPOINT ["java", "-jar", "app_step-test.jar"]
